\select@language {american}
\select@language {american}
\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo Geral}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{2}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o da Monografia}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Fundamentos em Redes \IeC {\'O}pticas WDM}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Contextualiza\IeC {\c c}\IeC {\~a}o das Redes WDM}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Divis\IeC {\~a}o e Multiplexa\IeC {\c c}\IeC {\~a}o de Comprimento de Onda}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Caminho \IeC {\'O}ptico}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Agrega\IeC {\c c}\IeC {\~a}o de Tr\IeC {\'a}fego}{6}{section.2.4}
\contentsline {section}{\numberline {2.5}Convers\IeC {\~a}o de Comprimento de Onda}{6}{section.2.5}
\contentsline {section}{\numberline {2.6}Roteamento e Aloca\IeC {\c c}\IeC {\~a}o de Comprimentos de Onda}{7}{section.2.6}
\contentsline {section}{\numberline {2.7}Tr\IeC {\'a}fego em Redes \IeC {\'O}pticas}{7}{section.2.7}
\contentsline {chapter}{\numberline {3}WDMSim}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Algoritmo RWA}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Execu\IeC {\c c}\IeC {\~a}o}{9}{section.3.2}
\contentsline {chapter}{\numberline {4}Especifica\IeC {\c c}\IeC {\~a}o}{12}{chapter.4}
\contentsline {section}{\numberline {4.1}Interface Gr\IeC {\'a}fica}{12}{section.4.1}
\contentsline {section}{\numberline {4.2}Servidor de Simula\IeC {\c c}\IeC {\~a}o}{12}{section.4.2}
\contentsline {section}{\numberline {4.3}Requisitos}{13}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Nova Simula\IeC {\c c}\IeC {\~a}o}{13}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Abrir Simula\IeC {\c c}\IeC {\~a}o}{13}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Salvar Simula\IeC {\c c}\IeC {\~a}o}{13}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Novo Roteador}{14}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Novo Enlace}{14}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Remover Roteador}{14}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Editar Roteador}{14}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}Editar Enlaces}{15}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}Manter Chamadas}{15}{subsection.4.3.9}
\contentsline {subsection}{\numberline {4.3.10}Executar Simula\IeC {\c c}\IeC {\~a}o}{16}{subsection.4.3.10}
\contentsline {subsection}{\numberline {4.3.11}Gerar Estat\IeC {\'\i }sticas}{16}{subsection.4.3.11}
\contentsline {subsection}{\numberline {4.3.12}Adicionar Classe RWA}{16}{subsection.4.3.12}
\contentsline {subsection}{\numberline {4.3.13}Exportar Resultados}{17}{subsection.4.3.13}
\contentsline {subsection}{\numberline {4.3.14}Recuperar Trace}{17}{subsection.4.3.14}
\contentsline {section}{\numberline {4.4}Requisitos Desej\IeC {\'a}veis}{17}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Criar Topologia Comuns}{17}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Copiar e Colar Roteadores}{18}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Deletar Simula\IeC {\c c}\IeC {\~a}o}{18}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Salvar e Recuperar Simula\IeC {\c c}\IeC {\~a}o}{18}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Atalhos}{18}{subsection.4.4.5}
\contentsline {subsection}{\numberline {4.4.6}Barra de Rolagem}{18}{subsection.4.4.6}
\contentsline {subsection}{\numberline {4.4.7}Adi\IeC {\c c}\IeC {\~a}o de Eventos}{19}{subsection.4.4.7}
\contentsline {chapter}{\numberline {5}Projeto e Arquitetura}{20}{chapter.5}
\contentsline {section}{\numberline {5.1}Pacotes da Aplica\IeC {\c c}\IeC {\~a}o}{20}{section.5.1}
\contentsline {section}{\numberline {5.2}Padr\IeC {\~o}es de Projeto}{21}{section.5.2}
\contentsline {section}{\numberline {5.3}Implementa\IeC {\c c}\IeC {\~a}o}{22}{section.5.3}
\contentsline {chapter}{\numberline {6}Valida\IeC {\c c}\IeC {\~a}o}{24}{chapter.6}
\contentsline {section}{\numberline {6.1}Fatores de Qualidade}{24}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Manutenibilidade}{24}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Interoperabilidade}{24}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Reusabilidade}{25}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Portabilidade}{25}{subsection.6.1.4}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~a}o}{26}{chapter.7}
\contentsline {chapter}{Refer\^encias}{27}{section*.8}
\contentsline {chapter}{\numberline {A}Manual de Usu\IeC {\'a}rio}{29}{appendix.A}
