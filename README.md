# Leia-Me #

Este repositório contém o código e documentação da ferramenta WDMSimulator, uma interface gráfica integrada para execução de simulações de redes ópticas com Divisão e Multiplexação de comprimento de onda. 


### O repositório contém: ###

* Ante-Projeto: contém os principais requisitos funcionais planejados para o software.
* Implementação: arquivos fontes, classes java e bibliotecas para a configuração e execução do simulador
* Monografia: documentos que descrevem os objetivos e as bases teóricas para o entendimento do projeto.

### Execução ###

 Para executar o simulador: 

* Importar o projeto.
```
$ git clone https://guihred@bitbucket.org/guihred/wdmsim.git
```

* Executar comando de iniciação

```
$ [repo dir]/Implementação/WDMSimulator/runAplication.bat
```

* Acessar link do simulador em um browser com suporte a HTML5

```
$ http://localhost:8888/WDMSimulator.html
```

* Configuração

  A execução irá abrir uma tela de execução, onde é possível observar o log de execução do simulador.

* Dependência

  Para executar a simulação, é preciso ter instalado o ambiente java JDK 7 ou mais.

* Configurações de Database 

  O Sistema utiliza a biblioteca H2 embarcada que, portanto, não necessita de instalação prévia.


### Autoria e Administração ###

* O administrador é Guilherme Fernandes 

--guih.red@gmail.com