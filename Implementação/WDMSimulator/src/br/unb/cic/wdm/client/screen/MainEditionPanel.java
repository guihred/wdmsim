package br.unb.cic.wdm.client.screen;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import br.unb.cic.wdm.client.elements.NetworkUtils;
import br.unb.cic.wdm.client.screen.functions.EditLinksFunction;
import br.unb.cic.wdm.client.screen.functions.EditNodeFunction;
import br.unb.cic.wdm.client.screen.functions.ValueChangeFormatter;
import br.unb.cic.wdm.shared.domain.Calls;
import br.unb.cic.wdm.shared.domain.Node;

import com.chap.links.client.events.SelectHandler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Panel that eases the network elements edition
 * 
 * @author Guilherme
 * 
 */
public class MainEditionPanel extends SelectHandler {
    
    /**
     * Reference to the edition panel
     */
    public static final MainEditionPanel instance = new MainEditionPanel();
    
    /**
     * @return reference to the edition panel
     */
    public static MainEditionPanel getInstance() {
        return instance;
    }
    
    /**
     * Table with the created calls
     */
    private final FlexTable    gridCalls   = new FlexTable();
    
    /**
     * Current selected node's id
     */
    private int                idNode;
    
    /**
     * Field with the current selected node's id
     */
    private TextBox            idNodeBox;
    
    /**
     * 
     */
    private final NetworkUtils mainNetwork = NetworkUtils.getMainNetwork();
    
    /**
     * Field with the current selected node's name
     */
    private TextBox            textNodeBox;
    
    /**
     * Constructor
     */
    private MainEditionPanel() {
        final DecoratorPanel formNode = createFormNode();
        final DecoratorPanel formCalls = createFormCalls();
        VerticalPanel panel = new VerticalPanel();
        panel.add(formNode);
        panel.add(formCalls);
        
        RootPanel.get().add(panel, MainBoard.getNetworkWidth() + 30, 0);
        
    }
    
    /**
     * @return panel calls's information
     */
    private DecoratorPanel createFormCalls() {
        updateCalls();
        DecoratorPanel decoratorPanel = new DecoratorPanel();
        decoratorPanel.setWidget(gridCalls);
        setStyle(decoratorPanel.getElement().getStyle());
        return decoratorPanel;
    }
    
    /**
     * @return panel with the selected node
     */
    private DecoratorPanel createFormNode() {
        
        FlexTable grid = new FlexTable();
        grid.setWidget(0, 0, new Label(NODE_INFORMATION));
        grid.getFlexCellFormatter().setColSpan(0, 0, 2);
        grid.setWidget(1, 0, new Label(ID));
        grid.setWidget(2, 0, new Label(TEXT));
        
        idNodeBox = new TextBox();
        idNodeBox.setName("idNode");
        idNodeBox.setTitle(ID);
        idNodeBox.setReadOnly(true);
        grid.setWidget(1, 1, idNodeBox);
        
        textNodeBox = new TextBox();
        textNodeBox.setName("textNode");
        textNodeBox.setTitle(TEXT);
        // textNodeBox.setReadOnly(true);
        textNodeBox.addChangeHandler(new ChangeHandler() {
            
            @Override
            public void onChange(ChangeEvent event) {
                Node selectedNode = mainNetwork.getSelectedNode();
                if (selectedNode != null) {
                    selectedNode.setText(textNodeBox.getValue());
                    mainNetwork.updateNodes();
                }
            }
        });
        grid.setWidget(2, 1, textNodeBox);
        
        grid.setWidget(3, 1, new Button(ALTER, new EditNodeFunction()));
        
        Button editLinksButton = new Button(ALTER_LINKS, new EditLinksFunction());
        editLinksButton.setTitle(ALTER_LINKS);
        grid.setWidget(4, 1, editLinksButton);
        
        final CheckBox checkBox = new CheckBox(SHOW_WEIGHT);
        checkBox.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.setShowWeight(checkBox.getValue());
                mainNetwork.updateLinks();
            }
        });
        grid.setWidget(5, 1, checkBox);
        
        DecoratorPanel decoratorPanel = new DecoratorPanel();
        decoratorPanel.setWidget(grid);
        
        setStyle(decoratorPanel.getElement().getStyle());
        
        return decoratorPanel;
    }
    
    @Override
    public void onSelect(SelectEvent event) {
        Node selectedNode = mainNetwork.getSelectedNode();
        if (selectedNode != null) {
            idNode = selectedNode.getId();
            idNodeBox.setValue(((Integer) idNode).toString());
            textNodeBox.setValue(selectedNode.getText());
        }
    }
    
    /**
     * Sets the default style
     * 
     * @param style
     */
    private void setStyle(Style style) {
        style.setPadding(9, Unit.PX);
        style.setBackgroundColor("rgb(239,241,241)");
        style.setBorderWidth(1, Unit.PX);
        style.setBorderStyle(BorderStyle.SOLID);
        style.setBorderColor("rgb(128,128,128)");
        style.setWidth(MainBoard.getEditPanelWidth(), Unit.PX);
    }
    
    /**
     * Updates the state of the shown calls in the
     * panel
     */
    public void updateCalls() {
        gridCalls.clear();
        gridCalls.setWidget(0, 0, new Label(CALLS_INFORMATION));
        gridCalls.getFlexCellFormatter().setColSpan(0, 0, 4);
        gridCalls.setWidget(1, 0, new Label(RATE));
        Label cosLabel = new Label(COS);
        cosLabel.setTitle(COS_HINT);
        gridCalls.setWidget(1, 1, cosLabel);
        gridCalls.setWidget(1, 2, new Label(WEIGHT_CALL));
        Label holdingLabel = new Label(HOLDING_TIME);
        holdingLabel.setTitle(HOLDING_TIME_HINT);
        gridCalls.setWidget(1, 3, holdingLabel);
        
        PushButton button = new PushButton(new Image("./add.png"));
        button.setTitle(NEW_CALL);
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.getSimulation().getCalls().add(new Calls());
                MainMenu.disableItems();
                updateCalls();
            }
        });
        /**/
        gridCalls.setWidget(1, 4, button);
        
        List<Calls> calls = mainNetwork.getSimulation().getCalls();
        for (int i = 0; i < calls.size(); i++) {
            final Calls call = calls.get(i);
            
            final TextBox rate = new TextBox();
            rate.setValue(call.getRate() + "");
            rate.setSize("40px", "15px");
            rate.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    call.setRate(number.intValue());
                }
            });
            gridCalls.setWidget(i + 2, 0, rate);
            
            final TextBox cos = new TextBox();
            cos.setValue(call.getCos() + "");
            cos.setMaxLength(4);
            cos.setSize("20px", "15px");
            cos.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    call.setCos(number.intValue());
                }
            });
            gridCalls.setWidget(i + 2, 1, cos);
            
            final TextBox weight = new TextBox();
            weight.setValue(call.getWeight() + "");
            weight.setMaxLength(8);
            weight.setSize("30px", "15px");
            weight.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    call.setWeight(number.intValue());
                }
            });
            gridCalls.setWidget(i + 2, 2, weight);
            final TextBox holding = new TextBox();
            holding.setValue("" + call.getHoldingTime());
            holding.setMaxLength(3);
            holding.setSize("20px", "15px");
            holding.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    call.setHoldingTime(number.intValue());
                }
            });
            gridCalls.setWidget(i + 2, 3, holding);
            
            final int j = i;
            PushButton excludeCall = new PushButton(new Image("./cancel.png"));
            excludeCall.setSize("15px", "15px");
            excludeCall.setTitle(REMOVE_CALL);
            excludeCall.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    mainNetwork.getSimulation().getCalls().remove(j);
                    updateCalls();
                }
            });
            gridCalls.setWidget(i + 2, 4, excludeCall);
        }
    }
}
