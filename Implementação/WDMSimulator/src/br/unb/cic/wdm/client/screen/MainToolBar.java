package br.unb.cic.wdm.client.screen;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;
import br.unb.cic.wdm.client.elements.NetworkUtils;
import br.unb.cic.wdm.client.screen.functions.EditNodeFunction;

import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Class with lateral tool bar for network edition
 * 
 * @author Guilherme
 * 
 */
public class MainToolBar {
    
    /**
     * Unique reference to the tool bar
     */
    public static MainToolBar mainToolBar;
    
    /**
     * Creates the tool bar
     */
    public static void createToolBar() {
        mainToolBar = new MainToolBar();
    }
    
    private final NetworkUtils mainNetwork = NetworkUtils.getMainNetwork();
    
    /**
     * Constructor
     */
    private MainToolBar() {
        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setVertical(true);
        toolStrip.setHeight(MainBoard.getNetworkHeight());
        toolStrip.setWidth(30);
        toolStrip.setTop(30);
        
        ToolStripButton addNodeButton = getAddNodeButton();
        toolStrip.addButton(addNodeButton);
        
        toolStrip.addResizer();
        
        ToolStripButton addLinkButton = getAddLinkButton();
        toolStrip.addButton(addLinkButton);
        
        toolStrip.addResizer();
        
        ToolStripButton removeNodeButton = getRemoveNodeButton();
        toolStrip.addButton(removeNodeButton);
        
        toolStrip.addResizer();
        
        ToolStripButton editNodeButton = getEditNodeButton();
        toolStrip.addButton(editNodeButton);
        
        // push all buttons to the top
        toolStrip.addFill();
        toolStrip.draw();
    }
    
    /**
     * @return link addition button
     */
    private ToolStripButton getAddLinkButton() {
        ToolStripButton addLinkButton = new ToolStripButton();
        addLinkButton.setPrompt(NEW_LINK);
        addLinkButton.setIcon("/../link.png");
        addLinkButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.addLink();
            }
        });
        return addLinkButton;
    }
    
    /**
     * @return button for node addition
     */
    private ToolStripButton getAddNodeButton() {
        ToolStripButton addNodeButton = new ToolStripButton();
        addNodeButton.setPrompt(NEW_NODE);
        addNodeButton.setIcon("[SKIN]/actions/add.png");
        
        addNodeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.addNode();
            }
        });
        return addNodeButton;
    }
    
    /**
     * @return button for execute node's edition
     */
    private ToolStripButton getEditNodeButton() {
        ToolStripButton editNodeButton = new ToolStripButton();
        editNodeButton.setPrompt(EDIT_NODE);
        editNodeButton.setIcon("[SKIN]/actions/edit.png");
        editNodeButton.addClickHandler(new EditNodeFunction());
        return editNodeButton;
    }
    
    /**
     * @return button for removing selected node
     */
    private ToolStripButton getRemoveNodeButton() {
        ToolStripButton removeNodeButton = new ToolStripButton();
        removeNodeButton.setPrompt(REMOVE_NODE);
        removeNodeButton.setIcon("[SKIN]/actions/remove.png");
        removeNodeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.removeNode();
            }
        });
        return removeNodeButton;
    }
    
}