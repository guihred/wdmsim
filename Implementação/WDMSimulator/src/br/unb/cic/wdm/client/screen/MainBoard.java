package br.unb.cic.wdm.client.screen;

import br.unb.cic.wdm.client.elements.NetworkUtils;

import com.chap.links.client.Network;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * 
 * Class responsible for creating the main board
 * with the modeled network
 * 
 * @author Guilherme
 * 
 */
public class MainBoard implements IsWidget {
    
    /**
     * Unique reference to the main board
     */
    private static MainBoard mainBoard;
    
    /**
     * @return default height for the dialog boxes
     */
    public final static int getDefaultDialogBoxHeight() {
        return Window.getClientWidth() * 1 / 5;
    }
    
    /**
     * @return default width for the dialog boxes
     */
    public final static int getDefaultDialogBoxWidth() {
        return Window.getClientWidth() * 4 / 10;
    }
    
    /**
     * @return default width for the edition panel
     */
    public final static int getEditPanelWidth() {
        return Window.getClientWidth() - MainBoard.getNetworkWidth() - 30;
    }
    
    /**
     * @return main board instance
     */
    public static final MainBoard getInstance() {
        if (mainBoard == null) {
            mainBoard = new MainBoard();
        }
        return mainBoard;
    }
    
    /**
     * @return default height of the board with
     *         the modeled network
     */
    public final static int getNetworkHeight() {
        return Window.getClientHeight() - 60;
    }
    
    /**
     * @return default width of the board with the
     *         modeled network
     */
    public final static int getNetworkWidth() {
        return Window.getClientWidth() * 4 / 5;
    }
    
    /**
     * Unique reference to the network
     */
    private final NetworkUtils networkUtils = NetworkUtils.getMainNetwork();
    
    /**
     * Constructor
     */
    private MainBoard() {
    }
    
    @Override
    public Network asWidget() {
        return getNetworkUtils().getNetwork();
    }
    
    /**
     * @return networkUtils
     */
    public NetworkUtils getNetworkUtils() {
        return networkUtils;
    }
    
}
