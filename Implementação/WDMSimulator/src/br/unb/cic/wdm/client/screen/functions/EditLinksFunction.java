package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Function that displays to the user the link's
 * information
 * 
 * @author Guilherme
 * 
 */
public class EditLinksFunction extends FunctionCommand {
    
    /**
     * Table with the link's information
     */
    private final FlexTable linkFormGrid = new FlexTable();
    /**
     * Selected node
     */
    private Node            selectedNode;
    
    /**
     * @return dialog box with the link's
     *         information
     */
    private DialogBox createDialogBox() {
        dialogBox = new DialogBox();
        List<Link> linksById = mainNetwork.getLinksById(selectedNode.getId());
        linkFormGrid.clear();
        linkFormGrid.setWidget(0, 0, new Label(LINK_INFORMATION));
        linkFormGrid.getFlexCellFormatter().setColSpan(0, 0, 4);
        linkFormGrid.setWidget(1, 0, new Label(ID));
        linkFormGrid.setWidget(1, 2, new Label(TO));
        linkFormGrid.setWidget(1, 1, new Label(FROM));
        linkFormGrid.setWidget(1, 3, new Label(DELAY));
        linkFormGrid.setWidget(1, 4, new Label(WEIGHT));
        linkFormGrid.setWidget(1, 5, new Label(BANDWIDTH));
        
        int i = 0;
        for (; i < linksById.size(); i++) {
            
            final Link link = linksById.get(i);
            
            final TextBox idLink = new TextBox();
            idLink.setValue(((Integer) link.getId()).toString());
            idLink.setMaxLength(3);
            idLink.setEnabled(false);
            idLink.setSize("20px", "15px");
            
            linkFormGrid.setWidget(i + 2, 0, idLink);
            
            final TextBox toLink = new TextBox();
            toLink.setValue(((Integer) link.getTo()).toString());
            toLink.setMaxLength(3);
            toLink.setSize("20px", "15px");
            toLink.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    link.setTo(number.intValue());
                }
            });
            linkFormGrid.setWidget(i + 2, 2, toLink);
            
            final TextBox fromLink = new TextBox();
            fromLink.setValue(((Integer) link.getFrom()).toString());
            fromLink.setMaxLength(3);
            fromLink.setSize("20px", "15px");
            fromLink.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    link.setFrom(number.intValue());
                }
            });
            linkFormGrid.setWidget(i + 2, 1, fromLink);
            
            final TextBox delayLink = new TextBox();
            delayLink.setValue(NumberFormat.getFormat("#0.0").format(link.getDelay().doubleValue()));
            delayLink.setMaxLength(4);
            delayLink.setSize("30px", "15px");
            delayLink.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    link.setDelay(number.doubleValue());
                }
            });
            linkFormGrid.setWidget(i + 2, 3, delayLink);
            
            final TextBox weightLink = new TextBox();
            weightLink.setValue(link.getWeight().toString());
            weightLink.setMaxLength(8);
            weightLink.setSize("40px", "15px");
            weightLink.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    link.setWeight(number.intValue());
                }
            });
            linkFormGrid.setWidget(i + 2, 4, weightLink);
            
            final TextBox bandwidthLink = new TextBox();
            bandwidthLink.setValue(link.getBandwidth().toString());
            bandwidthLink.setMaxLength(8);
            bandwidthLink.setSize("40px", "15px");
            bandwidthLink.addChangeHandler(new ValueChangeFormatter() {
                @Override
                public void setValue(Number number) {
                    link.setBandwidth(number.intValue());
                }
            });
            linkFormGrid.setWidget(i + 2, 5, bandwidthLink);
        }
        Button alterButton = new Button(ALTER, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mainNetwork.updateLinks();
                dialogBox.hide();
            }
        });
        linkFormGrid.setWidget(i + 2, 6, alterButton);
        dialogBox.setWidget(linkFormGrid);
        setStyle(dialogBox.getElement().getStyle());
        // Return the dialog box
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        selectedNode = mainNetwork.getSelectedNode();
        if (selectedNode != null) {
            final DialogBox dialogBox = createDialogBox();
            dialogBox.setModal(true);
            dialogBox.setAnimationEnabled(true);
            dialogBox.setAutoHideEnabled(true);
            dialogBox.setTitle(LINK_CONFIG);
            dialogBox.center();
            
            dialogBox.show();
        }
    }
}
