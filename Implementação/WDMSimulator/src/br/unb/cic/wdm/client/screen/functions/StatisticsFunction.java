package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;
import br.unb.cic.wdm.client.screen.MainMenu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.SC;

/**
 * Function responsible for displaying the link to
 * download statistics
 * 
 * @author Guilherme
 */
public class StatisticsFunction extends FunctionCommand {
    /**
     * @return creates dialog box with the link to
     *         download s
     */
    
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(GENERATE_ESTATISTICS);
        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);
        
        HTML details = new HTML(CHOOSE_FILE);
        dialogContents.add(details);
        dialogContents.setCellHorizontalAlignment(details, HasHorizontalAlignment.ALIGN_CENTER);
        
        final Anchor anchor = new Anchor(SAVE);
        if (!MainMenu.isItemsEnabled()) {
            SC.warn(SIMULATION_NOT_RUN);
            dialogBox.hide();
        } else {
            String moduleBaseURL = GWT.getModuleBaseURL();
            String url = moduleBaseURL.replace("wdmsimulator/", "") + "stats?chave=" + mainNetwork.getIdSimulation();
            anchor.setHref(url);
            
        }
        
        dialogContents.add(anchor);
        setStyle(dialogBox.getElement().getStyle());
        return dialogBox;
    }
    
    @Override
    public void execute() {
        dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(CHOOSE);
        dialogBox.center();
        dialogBox.show();
    }
}
