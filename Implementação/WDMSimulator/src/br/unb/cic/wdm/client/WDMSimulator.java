package br.unb.cic.wdm.client;

import org.fusesource.restygwt.client.Defaults;

import br.unb.cic.wdm.client.screen.MainBoard;
import br.unb.cic.wdm.client.screen.MainMenu;
import br.unb.cic.wdm.client.screen.MainToolBar;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.visualization.client.VisualizationUtils;

/**
 * 
 * Class responsible for creating the main screen
 * of the simulator
 * 
 * @author Guilherme
 */
public class WDMSimulator implements EntryPoint {
    
    @Override
    public void onModuleLoad() {
        MainMenu.createMenu();
        MainToolBar.createToolBar();
        RootPanel.get().getElement().getStyle().setPosition(Position.STATIC);
        Defaults.setDateFormat(null);
        MainBoard quadroPrincipal = MainBoard.getInstance();
        VisualizationUtils.loadVisualizationApi(quadroPrincipal.getNetworkUtils());
        
    }
}