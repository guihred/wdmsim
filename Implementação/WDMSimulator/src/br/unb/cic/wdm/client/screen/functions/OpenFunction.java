package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.shared.FileResourceAsync;
import br.unb.cic.wdm.shared.domain.Simulation;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.SC;

/**
 * Function that allows users to submit a XML file
 * with parameter of the simulation
 * 
 * @author Guilherme
 */
public class OpenFunction extends FunctionCommand {
    
    /**
     * @return dialog box with a link for file
     *         submission
     */
    private DialogBox createDialogBox() {
        final FileResourceAsync fileResourceAsync = FileResourceAsync.Util.get();
        // Create a dialog box and set the caption
        // text
        final FormPanel formPanel = new FormPanel();
        formPanel.setMethod(FormPanel.METHOD_POST);
        formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanel.setAction("/upload");
        
        final DialogBox dialogBox = new DialogBox();
        dialogBox.setText(OPEN_CONFIG_FILE);
        dialogBox.setWidget(formPanel);
        
        final VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        formPanel.setWidget(dialogContents);
        
        HTML details = new HTML(CHOOSE_FILE);
        dialogContents.add(details);
        dialogContents.setCellHorizontalAlignment(details, HasHorizontalAlignment.ALIGN_CENTER);
        
        final FileUpload fileUpload = new FileUpload();
        fileUpload.setEnabled(true);
        fileUpload.setName("upload");
        fileUpload.getElement().setAttribute("accept", "text/xml");
        
        final Hidden hidden = new Hidden("client");
        dialogContents.add(hidden);
        resourceAsync.createUploadUrl(new MethodCallback<String>() {
            
            @Override
            public void onFailure(Method method, Throwable exception) {
                SC.say(exception.getLocalizedMessage());
            }
            
            @Override
            public void onSuccess(Method method, String result) {
                hidden.setValue(result);
            }
        });
        Button openButton = new Button(OPEN, new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                formPanel.submit();
            }
        });
        // openButton.set
        formPanel.addSubmitHandler(new SubmitHandler() {
            
            @Override
            public void onSubmit(SubmitEvent event) {
                if (hidden.getValue() == null || fileUpload.getFilename() == null || fileUpload.getFilename().isEmpty()) {
                    SC.warn(OPEN_ERROR);
                    event.cancel();
                }
            }
        });
        
        formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                dialogBox.hide();
                fileResourceAsync.openSimulation(Long.valueOf(hidden.getValue()), new MethodCallback<Simulation>() {
                    
                    @Override
                    public void onFailure(Method method, Throwable caught) {
                        SC.say(caught.getLocalizedMessage());
                    }
                    
                    @Override
                    public void onSuccess(Method method, Simulation result) {
                        if (result != null)
                            mainNetwork.setSimulation(result);
                    }
                });
            }
        });
        dialogContents.add(fileUpload);
        dialogContents.add(openButton);
        
        setStyle(dialogBox.getElement().getStyle());
        
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        if (dialogBox == null) {
            dialogBox = createDialogBox();
            dialogBox.setModal(true);
            dialogBox.setAnimationEnabled(true);
            dialogBox.setAutoHideEnabled(true);
            dialogBox.setTitle(CHOOSE);
            dialogBox.center();
        }
        dialogBox.show();
    }
}
