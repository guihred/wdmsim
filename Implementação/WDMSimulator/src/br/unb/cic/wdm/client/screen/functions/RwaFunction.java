package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.SC;

/**
 * Function responsible for executing logic of
 * addition of Rwa class.
 * 
 * @author Guilherme
 * 
 */
public class RwaFunction extends FunctionCommand {
    
    /**
     * Regex of key-words in Java Language
     */
    private final static String regex       = "(private|protected|public|abstract|class|extends|final|implements|interface|native|new|static|strictfp|synchronized|transient|volatile|break|case|continue|default|do|else|for|if|instanceof|return|switch|while|assert|catch|finally|throw|throws|try|import|package|boolean|byte|char|double|float|int|long|short|super|this|void)";
    /**
     * Standard RWA Class for user implementation
     */
    private static final String standardRwa = "<pre>import br.unb.cic.wdm.server.wdmsim.Flow;"
                                                    + "<br/>import br.unb.cic.wdm.server.wdmsim.PhysicalTopology;"
                                                    + "<br/>import br.unb.cic.wdm.server.wdmsim.VirtualTopology;"
                                                    + "<br/>import br.unb.cic.wdm.server.wdmsim.util.Dijkstra;"
                                                    + "<br/>import br.unb.cic.wdm.server.wdmsim.util.WeightedGraph;"
                                                    + "<br/>public class CustomRWA implements RWA {"
                                                    + "<br/>        private ControlPlaneForRWA cp;"
                                                    + "<br/>        private WeightedGraph      graph;"
                                                    + "<br/>        private PhysicalTopology   pt;"
                                                    + "<br/>        private VirtualTopology    vt;<br/>"
                                                    + "<br/>        @Override"
                                                    + "<br/>        public void flowArrival(Flow flow) {"
                                                    + "<br/>        }<br/>"
                                                    + "<br/>        @Override"
                                                    + "<br/>        public void flowDeparture(long id) {"
                                                    + "<br/>        }"
                                                    + "<br/>       "
                                                    + "<br/>        public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp) {"
                                                    + "<br/>           this.pt = pt;" + "<br/>           this.vt = vt;"
                                                    + "<br/>           this.cp = cp;"
                                                    + "<br/>           this.graph = pt.getWeightedGraph();"
                                                    + "<br/>        }" + "<br/>}</pre>";
    
    /**
     * Rich text area containing Rwa code
     */
    final RichTextArea          rwaClass    = new RichTextArea();
    
    /**
     * Creates dialog box with text field to write
     * the RWA class
     * 
     * @return caixa de diálogo
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(CUSTOM_RWA);
        
        final VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setWidth("100%");
        
        rwaClass.setHeight("400px");
        rwaClass.setHTML(standardRwa);
        rwaClass.setWidth("100%");
        rwaClass.getElement().setAttribute("spellcheck", "false");
        dialogContents.add(rwaClass);
        Button openButton = new Button(ADD, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // Simulation simulation =
                // NetworkUtils.getMainNetwork().getSimulation();
                dialogBox.hide();
                try {
                    rwaResourceAsync.addCustomRwa(rwaClass.getText(), new MethodCallback<List<String>>() {
                        
                        @Override
                        public void onFailure(Method method, Throwable exception) {
                            SC.say(exception.getLocalizedMessage());
                        }
                        
                        @Override
                        public void onSuccess(Method method, List<String> response) {
                            SC.say(response.toString());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            }
        });
        dialogContents.add(openButton);
        dialogBox.setWidget(dialogContents);
        Style style = dialogBox.getElement().getStyle();
        setStyle(style);
        style.setWidth(800, Unit.PX);
        rwaClass.getElement().getStyle().setBackgroundColor("rgb(255,255,255)");
        rwaClass.addDomHandler(new ChangeHandler() {
            
            @Override
            public void onChange(ChangeEvent event) {
                formatJava();
            }
            
        }, ChangeEvent.getType());
        return dialogBox;
    }
    
    @Override
    public void execute() {
        if (dialogBox == null) {
            dialogBox = createDialogBox();
            dialogBox.setModal(true);
            dialogBox.setSize("800px", "500px");
            dialogBox.setAnimationEnabled(true);
            dialogBox.setAutoHideEnabled(true);
            dialogBox.center();
            
        }
        
//        formatJava();
        dialogBox.show();
    }
    
    /**
     * Method that stilizes the key-words of java
     */
    private void formatJava() {
        String html = rwaClass.getHTML().replace("<b style=\"color:blue;\">", "").replace("</b>", "");
        StringBuilder text = new StringBuilder(html);
        String replace = RegExp.compile(regex, "g").replace(text.toString(), "<b style=\"color:blue;\">$1</b>");
        text.replace(0, text.length(), replace);
        rwaClass.setHTML(text.toString());
    }
}
