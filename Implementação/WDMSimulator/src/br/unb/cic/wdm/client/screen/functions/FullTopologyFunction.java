package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_NODE;

import java.util.ArrayList;
import java.util.List;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;

/**
 * Functio that crates a full topology
 * 
 * @author Guilherme
 * 
 */
public class FullTopologyFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.askforValue(QUESTION_NODE, new ValueCallback() {
            @Override
            public void execute(String value) {
                int nNodes = Integer.parseInt(value);
                List<Node> nodes = new ArrayList<Node>();
                for (int i = 0; i < nNodes; i++) {
                    nodes.add(mainNetwork.addNode());
                }
                for (int i = 0; i < nNodes; i++) {
                    
                    for (int j = i + 1; j < nNodes; j++) {
                        mainNetwork.addLink(new Link(nodes.get(i).getId(), nodes.get(j).getId()));
                        mainNetwork.addLink(new Link(nodes.get(j).getId(), nodes.get(i).getId()));
                        
                    }
                }
            }
        });
    }
    
}
