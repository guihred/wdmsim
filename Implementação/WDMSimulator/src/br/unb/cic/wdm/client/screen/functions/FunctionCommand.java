package br.unb.cic.wdm.client.screen.functions;

import br.unb.cic.wdm.client.elements.NetworkUtils;
import br.unb.cic.wdm.shared.FileResourceAsync;
import br.unb.cic.wdm.shared.RwaResourceAsync;
import br.unb.cic.wdm.shared.SimulationResourceAsync;
import br.unb.cic.wdm.shared.StatisticsResourceAsync;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

/**
 * Class generalizes the system's functions and
 * simplifies them to a single structure
 * 
 * @author Guilherme
 */
public abstract class FunctionCommand implements ClickHandler, com.smartgwt.client.widgets.menu.events.ClickHandler,
        com.smartgwt.client.widgets.events.ClickHandler, DoubleClickHandler, KeyDownHandler {
    /**
     * Dialog box to be created for use of certain
     * functions
     */
    protected DialogBox                     dialogBox;
    
    /**
     * Key Down event used to inform what key was
     * pressed
     */
    protected KeyDownEvent                  event;
    
    /**
     * Object that execute calls to the File
     * Resource REST service
     */
    protected final FileResourceAsync       fileResourceAsync       = FileResourceAsync.Util.get();
    
    /**
     * Referência única à instância de
     * NetworkUtils
     */
    protected final NetworkUtils            mainNetwork             = NetworkUtils.getMainNetwork();
    
    /**
     * Object that execute calls to the Simulation
     * Resource REST service
     */
    protected final SimulationResourceAsync resourceAsync           = SimulationResourceAsync.Util.get();
    
    /**
     * Object that execute calls to the Rwa
     * Resource REST service
     */
    protected final RwaResourceAsync        rwaResourceAsync        = RwaResourceAsync.Util.get();
    
    /**
     * Object that execute calls to the Statisitic
     * Resource REST service
     */
    protected final StatisticsResourceAsync statisticsResourceAsync = StatisticsResourceAsync.Util.get();
    
    /**
     * Abstract method that represents certain
     * logic to be implemented by a function
     */
    abstract void execute();
    
    @Override
    public void onClick(ClickEvent event) {
        this.execute();
    }
    
    @Override
    public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
        this.execute();
    }
    
    @Override
    public void onClick(MenuItemClickEvent event) {
        this.execute();
    }
    
    @Override
    public void onDoubleClick(DoubleClickEvent event) {
        this.execute();
    }
    
    @Override
    public void onKeyDown(KeyDownEvent event) {
        this.event = event;
        this.execute();
    }
    
    /**
     * Stylize the creation of the dialog box
     * 
     * @param style
     */
    protected void setStyle(Style style) {
        style.setPadding(9, Unit.PX);
        style.setBackgroundColor("rgb(239,241,241)");
        style.setBorderWidth(1, Unit.PX);
        style.setBorderStyle(BorderStyle.SOLID);
        style.setBorderColor("rgb(128,128,128)");
    }
    
}
