package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;

/**
 * Classe que disponibiliza ao usuário imagens e
 * esclarecimentos para ajudar na interação com o
 * simulador
 * 
 * @author Guilherme
 * 
 */
public class TutorialFunction extends FunctionCommand {
    
    /**
     * Imagem atual sendo chamada
     */
    private int                  current   = 0;
    /**
     * Imagem com primeiro passo
     */
    private final Image          image1    = new Image("./tutorial1.gif");
    /**
     * Imagem com segundo passo
     */
    private final Image          image2    = new Image("./tutorial2.gif");
    /**
     * Imagem com terceiro passo
     */
    private final Image          image3    = new Image("./tutorial3.gif");
    /**
     * Lista com as ações para o tutorial
     */
    private final List<Runnable> tutorials = new ArrayList<Runnable>();    ;
    
    /**
     * @return caixa de diálogo com o fluxo das
     *         imagens
     */
    private DialogBox createDialogBox() {
        dialogBox = new DialogBox();
        dialogBox.setText(TUTORIAL);
        final FlexTable table = new FlexTable();
        table.getFlexCellFormatter().setColSpan(0, 0, 3);
        Runnable tutor1 = new Runnable() {
            
            @Override
            public void run() {
                table.setWidget(0, 0, new Label(TUTORIAL_MSG_1));
                table.getFlexCellFormatter().setColSpan(0, 0, 3);
                table.getFlexCellFormatter().setColSpan(1, 0, 2);
                image1.setSize("400px", "300px");
                image1.setVisible(true);
                
                table.setWidget(1, 0, image1);
            }
        };
        
        Runnable tutor2 = new Runnable() {
            
            @Override
            public void run() {
                table.setWidget(0, 0, new Label(TUTORIAL_MSG_2));
                table.getFlexCellFormatter().setColSpan(0, 0, 3);
                table.getFlexCellFormatter().setColSpan(1, 0, 2);
                image2.setSize("400px", "300px");
                image2.setVisible(true);
                table.setWidget(1, 0, image2);
            }
        };
        
        Runnable tutor3 = new Runnable() {
            
            @Override
            public void run() {
                table.setWidget(0, 0, new Label(TUTORIAL_MSG_3));
                table.getFlexCellFormatter().setColSpan(0, 0, 2);
                table.getFlexCellFormatter().setColSpan(1, 0, 2);
                image3.setSize("400px", "300px");
                image3.setVisible(true);
                table.setWidget(1, 0, image3);
            }
        };
        tutorials.add(tutor1);
        tutorials.add(tutor2);
        tutorials.add(tutor3);
        tutorials.get(current).run();
        
        Anchor previous = new Anchor(PREVIOUS);
        table.setWidget(2, 0, previous);
        previous.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                setCurrent((getCurrent() + 2) % 3);
            }
        });
        
        Anchor next = new Anchor(NEXT);
        table.setWidget(2, 1, next);
        next.getElement().getStyle().setFloat(Float.RIGHT);
        next.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                setCurrent((getCurrent() + 1) % 3);
            }
        });
        
        dialogBox.setWidget(table);
        
        setStyle(dialogBox.getElement().getStyle());
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        if (dialogBox == null) {
            dialogBox = createDialogBox();
            dialogBox.setModal(true);
            dialogBox.setAnimationEnabled(true);
            dialogBox.setAutoHideEnabled(true);
            dialogBox.setTitle(TUTORIAL);
        }
        dialogBox.center();
        dialogBox.show();
    }
    
    /**
     * @return recupera índice de imagem atual
     */
    public int getCurrent() {
        return current;
    }
    
    /**
     * Permite setar o item de tutorial atual
     * 
     * @param current
     */
    public void setCurrent(int current) {
        this.current = current;
        tutorials.get(current).run();
    }
}
