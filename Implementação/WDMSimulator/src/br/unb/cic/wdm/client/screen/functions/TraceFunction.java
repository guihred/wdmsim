package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;
import br.unb.cic.wdm.client.screen.MainMenu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.SC;

/**
 * Classe responsável por disponibilizar para
 * download o arquivo de trace gerado pela
 * simulação
 * 
 * @author Guilherme
 * 
 */
public class TraceFunction extends FunctionCommand {
    
    /**
     * @return caixa de diálogo com o link para
     *         download do trace
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(EXPORT_TRACE);
        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);
        
        HTML details = new HTML(CHOOSE_FILE);
        dialogContents.add(details);
        dialogContents.setCellHorizontalAlignment(details, HasHorizontalAlignment.ALIGN_CENTER);
        
        final Anchor anchor = new Anchor("Salvar");
        if (!MainMenu.isItemsEnabled()) {
            SC.warn(SIMULATION_NOT_RUN);
            dialogBox.hide();
        } else {
            String moduleBaseURL = GWT.getModuleBaseURL();
            String url = moduleBaseURL.replace("wdmsimulator/", "") + "trace?chave=" + mainNetwork.getIdSimulation();
            anchor.setHref(url);
            
        }
        
        dialogContents.add(anchor);
        setStyle(dialogBox.getElement().getStyle());
        return dialogBox;
    }
    
    @Override
    public void execute() {
        dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(CHOOSE);
        dialogBox.center();
        dialogBox.show();
    }
}
