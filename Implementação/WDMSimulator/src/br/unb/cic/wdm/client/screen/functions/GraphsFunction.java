package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.client.screen.MainBoard;
import br.unb.cic.wdm.client.screen.MainMenu;
import br.unb.cic.wdm.shared.domain.Statistics;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BubbleChart;
import com.googlecode.gwt.charts.client.corechart.BubbleChartOptions;
import com.googlecode.gwt.charts.client.corechart.ColumnChart;
import com.googlecode.gwt.charts.client.corechart.ColumnChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.SizeAxis;
import com.googlecode.gwt.charts.client.options.VAxis;
import com.smartgwt.client.util.SC;

/**
 * Function responsible for illustrate the
 * simulation's statistics by graphs
 * 
 * @author Guilherme
 * 
 */
public class GraphsFunction extends FunctionCommand {
    
    /**
     * Graph option
     */
    private static final String CHART_OPTION = "chart";
    
    /**
     * Graph columns
     */
    private ColumnChart         areaChart;
    
    /**
     * Bubble graph
     */
    private BubbleChart         bubbleChart;
    
    /**
     * Panel where graphs are shown
     */
    private DockLayoutPanel     panel;
    /**
     * List of statistics for creating graphs
     */
    private List<Statistics>    statistics;
    
    /**
     * @return dialog box for graphs
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(GENERATE_ESTATISTICS);
        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogContents.add(getPanel());
        dialogBox.setWidget(dialogContents);
        
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        final RadioButton mbpOption = new RadioButton(CHART_OPTION, MBP);
        mbpOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                if (mbpOption.getValue()) {
                    drawMBPChart();
                }
            }
        });
        final RadioButton mbbrOption = new RadioButton(CHART_OPTION, MBBR);
        mbbrOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                if (mbbrOption.getValue()) {
                    drawMBBRChart();
                }
            }
        });
        final RadioButton callsOption = new RadioButton(CHART_OPTION, CALLS);
        callsOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                if (callsOption.getValue()) {
                    drawCallsChart();
                }
            }
            
        });
        
        horizontalPanel.add(mbpOption);
        horizontalPanel.add(mbbrOption);
        horizontalPanel.add(callsOption);
        dialogContents.add(horizontalPanel);
        setStyle(dialogBox.getElement().getStyle());
        dialogBox.getElement().getStyle().setWidth(MainBoard.getDefaultDialogBoxWidth() + 10, Unit.PX);
        // Return the dialog box
        return dialogBox;
    }
    
    /**
     * Function that draws graphs with calls made
     */
    private void drawCallsChart() {
        DataTable dataTable = DataTable.create();
        dataTable.addColumn(ColumnType.STRING, "ID");
        dataTable.addColumn(ColumnType.NUMBER, "From");
        dataTable.addColumn(ColumnType.NUMBER, "To");
        // dataTable.addColumn(ColumnType.STRING,
        // "Region");
        dataTable.addColumn(ColumnType.NUMBER, "Number of Calls");
        
        for (int i = 0; i < statistics.size(); i++) {
            String[] strings = statistics.get(i).getCalls().get(0);
            dataTable.addRow();
            dataTable.setValue(i, 0, " ");
            dataTable.setValue(i, 1, strings[0]);
            dataTable.setValue(i, 2, strings[1]);
            dataTable.setValue(i, 3, strings[2]);
            
        }
        // Set options
        BubbleChartOptions options = BubbleChartOptions.create();
        options.setTitle(GRAPH_CALLS);
        options.setBackgroundColor("#eef0f0");
        SizeAxis sizeAxis = SizeAxis.create();
        sizeAxis.setMaxSize(10);
        options.setSizeAxis(sizeAxis);
        options.setHAxis(HAxis.create("Pair 1"));
        options.setVAxis(VAxis.create("Pair 2"));
        // Draw the chart
        getBubbleChart().draw(dataTable, options);
    }
    
    /**
     * Function that draws graphs with mean
     * bandwidth blocking rate
     */
    private void drawMBBRChart() {
        DataTable dataTable = DataTable.create();
        
        dataTable.addColumn(ColumnType.STRING, COS_HINT);
        dataTable.addColumn(ColumnType.NUMBER, MBBR);
        
        for (int i = 0; i < statistics.size(); i++) {
            String[] strings = statistics.get(i).getMBBR().get(0);
            
            dataTable.addRow();
            for (int j = 0; j < strings.length; j++) {
                try {
                    dataTable.setValue(i, j, Double.valueOf(strings[j]));
                } catch (NumberFormatException e) {
                    dataTable.setValue(i, j, strings[j]);
                }
                
            }
        }
        // Set options
        ColumnChartOptions options = ColumnChartOptions.create();
        options.setTitle(MBBR_HINT);
        options.setBackgroundColor("#eef0f0");
        options.setIsStacked(true);
        options.setHAxis(HAxis.create(LOAD));
        options.setVAxis(VAxis.create(MBBR));
        // Window.style.setBackgroundColor("rgb(239,241,241)");
        
        getColumnChart().draw(dataTable, options);
    }
    
    /**
     * Function that draws graph with mean
     * blocking probability
     */
    private void drawMBPChart() {
        DataTable dataTable = DataTable.create();
        
        dataTable.addColumn(ColumnType.STRING, COS_HINT);
        dataTable.addColumn(ColumnType.NUMBER, MBP);
        
        for (int i = 0; i < statistics.size(); i++) {
            String[] strings = statistics.get(i).getMBP().get(0);
            
            dataTable.addRow();
            for (int j = 0; j < strings.length; j++) {
                try {
                    Double value = Double.valueOf(strings[j]);
                    dataTable.setValue(i, j, value);
                } catch (NumberFormatException e) {
                    dataTable.setValue(i, j, strings[j]);
                }
                
            }
        }
        // Set options
        ColumnChartOptions options = ColumnChartOptions.create();
        options.setTitle(MBP_HINT);
        options.setBackgroundColor("#eef0f0");
        options.setIsStacked(true);
        options.setHAxis(HAxis.create(LOAD));
        options.setVAxis(VAxis.create(MBP));
        // Window.
        // Draw the chart
        getColumnChart().draw(dataTable, options);
    }
    
    @Override
    public void execute() {
        if (dialogBox == null) {
            dialogBox = createDialogBox();
        }
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(GENERATE_ESTATISTICS);
        dialogBox.center();
        openStatistics();
        
        dialogBox.show();
    }
    
    /**
     * @return bubbleChart
     */
    private BubbleChart getBubbleChart() {
        if (bubbleChart == null) {
            bubbleChart = new BubbleChart();
            
        }
        if (getPanel().getWidgetCount() > 0) {
            panel.clear();
            panel.add(bubbleChart);
        }
        return bubbleChart;
    }
    
    /**
     * @return graphs of columns
     */
    private ColumnChart getColumnChart() {
        if (areaChart == null) {
            areaChart = new ColumnChart();
        }
        if (getPanel().getWidgetCount() > 0) {
            panel.clear();
            panel.add(areaChart);
        }
        
        return areaChart;
    }
    
    /**
     * @return main panel
     */
    private DockLayoutPanel getPanel() {
        if (panel == null) {
            panel = new DockLayoutPanel(Unit.PX);
            panel.setPixelSize(MainBoard.getDefaultDialogBoxWidth(), MainBoard.getDefaultDialogBoxHeight());
        }
        return panel;
    }
    
    /**
     * Execute remote call to recover the
     * statistics
     */
    private void openStatistics() {
        if (!MainMenu.isItemsEnabled()) {
            SC.warn(SIMULATION_NOT_RUN);
            dialogBox.hide();
        } else {
            Long idSimulation = mainNetwork.getIdSimulation();
            statisticsResourceAsync.openStatistics(idSimulation, new MethodCallback<List<Statistics>>() {
                
                @Override
                public void onFailure(Method method, Throwable exception) {
                    SC.warn(exception.getLocalizedMessage());
                }
                
                @Override
                public void onSuccess(Method method, List<Statistics> result) {
                    setStatistics(result);
                    System.out.println(result);
                    areaChart = null;
                    ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
                    chartLoader.loadApi(new Runnable() {
                        
                        @Override
                        public void run() {
                            getPanel().clear();
                            getPanel().add(getColumnChart());
                            getBubbleChart();
                        }
                    });
                    
                }
            });
        }
    }
    
    /**
     * @param statistics
     */
    private void setStatistics(List<Statistics> statistics) {
        this.statistics = statistics;
    }
}
