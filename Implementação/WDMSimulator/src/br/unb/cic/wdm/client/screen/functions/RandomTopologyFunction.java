package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_NODE;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;

/**
 * Function creates a random topology
 * 
 * @author Guilherme
 */
public class RandomTopologyFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.askforValue(QUESTION_NODE, new ValueCallback() {
            @Override
            public void execute(String value) {
                int nNodes = Integer.parseInt(value);
                List<Node> nodes = new ArrayList<Node>();
                for (int i = 0; i < nNodes; i++) {
                    nodes.add(mainNetwork.addNode());
                }
                Random random = new Random();
                for (int i = 0; i < nNodes; i++) {
                    int nextNode = random.nextInt(nNodes);
                    nextNode = nextNode == i ? random.nextInt(nNodes) : nextNode;
                    mainNetwork.addLink(new Link(nodes.get(i).getId(), nodes.get(nextNode).getId()));
                    mainNetwork.addLink(new Link(nodes.get(nextNode).getId(), nodes.get(i).getId()));
                }
            }
        });
    }
    
}
