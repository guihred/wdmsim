package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.shared.domain.Simulation;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.SC;

/**
 * 
 * Function that executes the saving operation of
 * the modeled simulation
 * 
 * @author Guilherme
 */
public class SaveFunction extends FunctionCommand {
    
    /**
     * @return creates a dialog box with the link
     *         to save simulation
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(SAVE_CONFIG_FILE);
        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);
        // new SimpleF
        HTML details = new HTML(CHOOSE_FILE);
        dialogContents.add(details);
        dialogContents.setCellHorizontalAlignment(details, HasHorizontalAlignment.ALIGN_CENTER);
        
        final Anchor anchor = new Anchor(SAVE);
        Simulation simulation = mainNetwork.getSimulation();
        fileResourceAsync.saveSimulation(simulation, new MethodCallback<Long>() {
            
            @Override
            public void onFailure(Method method, Throwable exception) {
                SC.say(exception.getLocalizedMessage());
            }
            
            @Override
            public void onSuccess(Method method, Long result) {
                String moduleBaseURL = GWT.getModuleBaseURL();
                String url = moduleBaseURL.replace("wdmsimulator/", "") + "download?chave=" + result;
                anchor.setHref(url);
                anchor.setTitle("simulation" + result + ".xml");
                anchor.setWordWrap(true);
                if (SC.isIE()) {
                    anchor.setTarget("_blank");
                    Window.open(url, "_blank", "resizable,scrollbars,status");
                }
            }
        });
        dialogContents.add(anchor);
        setStyle(dialogBox.getElement().getStyle());
        
        return dialogBox;
    }
    
    @Override
    public void execute() {
        dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(CHOOSE);
        dialogBox.center();
        dialogBox.show();
    }
}
