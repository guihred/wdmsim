package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.client.screen.MainMenu;
import br.unb.cic.wdm.shared.domain.Statistics;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;

/**
 * Function that displays statistic data obtained
 * from the simulation in tables
 * 
 * @author Guilherme
 */
public class TableFunction extends FunctionCommand {
    
    /**
     * Option of the radio buttons
     */
    private static final String CHART_OPTION = "chart";
    
    /**
     * Calls option
     */
    private RadioButton         callsOption;
    
    /**
     * Mean bandwidth blocking rate option
     */
    private RadioButton         mbbrOption;
    
    /**
     * Men blocking probability option
     */
    private RadioButton         mbpOption;
    
    /**
     * Main panel of the tables
     */
    private Panel               panel;
    
    /**
     * List of statistics used to create tables
     */
    private List<Statistics>    statistics;
    
    /**
     * @return created dialog box with the tables
     *         created
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(TABLE);
        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogContents.add(getPanel());
        dialogBox.setWidget(dialogContents);
        
        HorizontalPanel horizontalPanel = new HorizontalPanel();
        mbpOption = new RadioButton(CHART_OPTION, MBP);
        mbpOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                drawMBPTable();
            }
            
        });
        
        mbbrOption = new RadioButton(CHART_OPTION, MBBR);
        mbbrOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                drawMBBRTable();
            }
        });
        
        callsOption = new RadioButton(CHART_OPTION, CALLS);
        callsOption.addClickHandler(new ClickHandler() {
            
            @Override
            public void onClick(ClickEvent event) {
                drawCallsTable();
            }
            
        });
        
        horizontalPanel.add(mbpOption);
        horizontalPanel.add(mbbrOption);
        horizontalPanel.add(callsOption);
        dialogContents.add(horizontalPanel);
        setStyle(dialogBox.getElement().getStyle());
        
        // Return the dialog box
        return dialogBox;
    }
    
    /**
     * Creates a table with the number of calls
     */
    private void drawCallsTable() {
        dialogBox.setText(CALLS_NUMBER);
        FlexTable flexTable = new FlexTable();
        flexTable.setBorderWidth(1);
        
        for (Statistics stat : statistics) {
            String[] strings = stat.getCalls().get(0);
            Integer pair1 = Integer.valueOf(strings[0]);
            Integer pair2 = Integer.valueOf(strings[1]);
            flexTable.setWidget(pair1 + 1, 0, new Label(strings[0]));
            flexTable.setWidget(0, pair2, new Label(strings[1]));
            flexTable.setWidget(pair1 + 1, pair2, new Label(strings[2]));
        }
        getPanel().clear();
        getPanel().add(flexTable);
        dialogBox.center();
    }
    
    /**
     * Creates a table with the mean bandwidth
     * blocking rate
     */
    private void drawMBBRTable() {
        dialogBox.setText(MBBR_HINT);
        FlexTable flexTable = new FlexTable();
        flexTable.setBorderWidth(1);
        for (int i = 0; i < statistics.size(); i++) {
            String[] strings = statistics.get(i).getMBBR().get(0);
            flexTable.setWidget(i, 0, new Label(strings[0] + "-" + i));
            flexTable.setWidget(i, 1, new Label(strings[1]));
        }
        getPanel().clear();
        getPanel().add(flexTable);
        dialogBox.center();
    }
    
    /**
     * Creates a table with the Mean blocking
     * probability
     */
    private void drawMBPTable() {
        dialogBox.setText(MBP_HINT);
        FlexTable flexTable = new FlexTable();
        flexTable.setBorderWidth(1);
        for (int i = 0; i < statistics.size(); i++) {
            String[] strings = statistics.get(i).getMBP().get(0);
            flexTable.setWidget(i, 0, new Label(strings[0] + "-" + i));
            flexTable.setWidget(i, 1, new Label(strings[1]));
        }
        getPanel().clear();
        getPanel().add(flexTable);
        dialogBox.center();
    }
    
    @Override
    public void execute() {
        if (dialogBox == null) {
            dialogBox = createDialogBox();
        }
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(GENERATE_ESTATISTICS);
        dialogBox.center();
        openStatistics();
        
        dialogBox.show();
    }
    
    /**
     * @return main panel
     */
    private Panel getPanel() {
        if (panel == null) {
            panel = new FlowPanel();
        }
        return panel;
    }
    
    /**
     * Executes call to retrieve a simulation's
     * result
     */
    private void openStatistics() {
        if (!MainMenu.isItemsEnabled() && statistics == null) {
            SC.warn(SIMULATION_NOT_RUN);
            dialogBox.hide();
        } else if (!MainMenu.isItemsEnabled()) {
            SC.ask(SIMULATION_CHANGED, new BooleanCallback() {
                @Override
                public void execute(Boolean value) {
                    if (value) {
                        new ExecuteFunction().executeSimulation(dialogBox);
                    }
                }
            });
        } else {
            statisticsResourceAsync.openStatistics(mainNetwork.getIdSimulation(),
                    new MethodCallback<List<Statistics>>() {
                        
                        @Override
                        public void onFailure(Method method, Throwable caught) {
                            SC.warn(caught.getLocalizedMessage());
                        }
                        
                        @Override
                        public void onSuccess(Method method, List<Statistics> result) {
                            setStatistics(result);
                            if (mbbrOption.getValue()) {
                                drawMBBRTable();
                            } else if (mbpOption.getValue()) {
                                drawMBPTable();
                            } else if (callsOption.getValue()) {
                                drawCallsTable();
                            }
                        }
                    });
        }
    }
    
    /**
     * 
     * @param statistics
     */
    private void setStatistics(List<Statistics> statistics) {
        this.statistics = statistics;
    }
}
