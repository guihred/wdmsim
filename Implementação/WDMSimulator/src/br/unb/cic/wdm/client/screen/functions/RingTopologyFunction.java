package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_NODE;

import java.util.ArrayList;
import java.util.List;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;

/**
 * Function creates a ring topology
 * 
 * @author Guilherme
 */
public class RingTopologyFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.askforValue(QUESTION_NODE, new ValueCallback() {
            @Override
            public void execute(String value) {
                int nNodes = Integer.parseInt(value);
                List<Node> nodes = new ArrayList<Node>();
                for (int i = 0; i < nNodes; i++) {
                    nodes.add(mainNetwork.addNode());
                }
                for (int i = 0; i < nNodes; i++) {
                    mainNetwork.addLink(new Link(nodes.get(i).getId(), nodes.get((i + 1) % nNodes).getId()));
                    mainNetwork.addLink(new Link(nodes.get((i + 1) % nNodes).getId(), nodes.get(i).getId()));
                    
                }
            }
        });
    }
}
