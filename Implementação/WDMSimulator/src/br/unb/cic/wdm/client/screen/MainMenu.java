package br.unb.cic.wdm.client.screen;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;
import br.unb.cic.wdm.client.screen.functions.AboutFunction;
import br.unb.cic.wdm.client.screen.functions.CopyPasteFunction;
import br.unb.cic.wdm.client.screen.functions.DeleteTopologyFunction;
import br.unb.cic.wdm.client.screen.functions.ExecuteFunction;
import br.unb.cic.wdm.client.screen.functions.ExportFunction;
import br.unb.cic.wdm.client.screen.functions.FullTopologyFunction;
import br.unb.cic.wdm.client.screen.functions.GraphsFunction;
import br.unb.cic.wdm.client.screen.functions.NewSimulationFunction;
import br.unb.cic.wdm.client.screen.functions.OpenFunction;
import br.unb.cic.wdm.client.screen.functions.RandomTopologyFunction;
import br.unb.cic.wdm.client.screen.functions.RingTopologyFunction;
import br.unb.cic.wdm.client.screen.functions.RwaFunction;
import br.unb.cic.wdm.client.screen.functions.SaveFunction;
import br.unb.cic.wdm.client.screen.functions.StarTopologyFunction;
import br.unb.cic.wdm.client.screen.functions.StatisticsFunction;
import br.unb.cic.wdm.client.screen.functions.TableFunction;
import br.unb.cic.wdm.client.screen.functions.TraceFunction;
import br.unb.cic.wdm.client.screen.functions.TutorialFunction;

import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripMenuButton;

/**
 * Main menu shown above the main screen
 * 
 * @author Guilherme
 */
public class MainMenu {
    
    /**
     * Indicates if certain resource are enabled
     * or not
     */
    private static boolean  enabled = false;
    /**
     * Unique reference to the main menu
     */
    static MainMenu         mainMenu;
    /**
     * Item of menu that displays the results of
     * the simulation
     */
    private static MenuItem resultsItem;
    /**
     * Item of menu that displays the generates
     * files of the simulation
     */
    private static MenuItem traceItem;
    
    /**
     * Creates the menu
     */
    public static void createMenu() {
        mainMenu = new MainMenu();
    }
    
    /**
     * Disables the menu items
     */
    public static void disableItems() {
        if (enabled) {
            resultsItem.setEnabled(false);
            resultsItem.setTitle(EXECUTE_WARNING);
            traceItem.setEnabled(false);
            traceItem.setTitle(EXECUTE_WARNING);
            enabled = false;
        }
    }
    
    /**
     * Enables menu resources
     */
    public static void enableItems() {
        if (!enabled) {
            resultsItem.setEnabled(true);
            resultsItem.setTitle(null);
            traceItem.setEnabled(true);
            traceItem.setTitle(null);
            enabled = true;
        }
        
    }
    
    /**
     * @return if items are enabled or not
     */
    public static boolean isItemsEnabled() {
        return enabled;
    }
    
    /**
     * Constructor
     */
    
    private MainMenu() {
        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth(MainBoard.getNetworkWidth() + 30);
        
        ToolStripMenuButton menuButton = getFileMenu();
        toolStrip.addMenuButton(menuButton);
        
        toolStrip.addSeparator();
        
        ToolStripMenuButton editButton = getEditMenu();
        toolStrip.addMenuButton(editButton);
        
        toolStrip.addSeparator();
        
        ToolStripMenuButton simButton = getSimulationMenu();
        toolStrip.addMenuButton(simButton);
        
        toolStrip.addSeparator();
        
        ToolStripMenuButton statisticsButton = getStatisticsMenu();
        toolStrip.addMenuButton(statisticsButton);
        
        toolStrip.addSeparator();
        
        ToolStripMenuButton helpButton = getHelpMenu();
        toolStrip.addMenuButton(helpButton);
        
        toolStrip.addFill();
        
        toolStrip.draw();
    }
    
    /**
     * @return edition menu
     */
    private ToolStripMenuButton getEditMenu() {
        Menu menu = new Menu();
        
        MenuItem topologyItem = new MenuItem(CREATE_TOPOLOGY);
        Menu menuTopology = new Menu();
        
        MenuItem ringMenu = new MenuItem(RING);
        ringMenu.addClickHandler(new RingTopologyFunction());
        MenuItem starMenu = new MenuItem(STAR);
        starMenu.addClickHandler(new StarTopologyFunction());
        MenuItem randomMenu = new MenuItem(RANDOM);
        randomMenu.addClickHandler(new RandomTopologyFunction());
        MenuItem fullMenu = new MenuItem(FULL);
        fullMenu.addClickHandler(new FullTopologyFunction());
        
        menuTopology.setItems(ringMenu, starMenu, randomMenu, fullMenu);
        
        topologyItem.setSubmenu(menuTopology);
        
        MenuItem copyItem = new MenuItem(COPY_PASTE);
        copyItem.addClickHandler(new CopyPasteFunction());
        MenuItem deleteNodesItem = new MenuItem(DELETE);
        deleteNodesItem.addClickHandler(new DeleteTopologyFunction());
        
        menu.setItems(topologyItem, copyItem, deleteNodesItem);
        
        ToolStripMenuButton menuButton = new ToolStripMenuButton(EDIT, menu);
        menuButton.setWidth(100);
        return menuButton;
    }
    
    /**
     * @return file menu
     */
    private ToolStripMenuButton getFileMenu() {
        Menu menu = new Menu();
        
        MenuItem newItem = new MenuItem(NEW);
        newItem.addClickHandler(new NewSimulationFunction());
        MenuItem openItem = new MenuItem(OPEN);
        openItem.addClickHandler(new OpenFunction());
        MenuItem saveItem = new MenuItem(SAVE);
        saveItem.addClickHandler(new SaveFunction());
        
        menu.setItems(newItem, openItem, saveItem);
        
        ToolStripMenuButton menuButton = new ToolStripMenuButton(FILE, menu);
        menuButton.setWidth(100);
        return menuButton;
    }
    
    /**
     * @return help menu
     */
    private ToolStripMenuButton getHelpMenu() {
        Menu menu = new Menu();
        
        MenuItem tutorialItem = new MenuItem(TUTORIAL);
        tutorialItem.addClickHandler(new TutorialFunction());
        MenuItem aboutItem = new MenuItem(ABOUT);
        aboutItem.addClickHandler(new AboutFunction());
        menu.setItems(tutorialItem, aboutItem);
        
        ToolStripMenuButton menuButton = new ToolStripMenuButton(HELP, menu);
        menuButton.setWidth(100);
        return menuButton;
    }
    
    /**
     * @return simulation menu
     */
    private ToolStripMenuButton getSimulationMenu() {
        Menu menu = new Menu();
        
        MenuItem executeItem = new MenuItem(EXECUTE);
        executeItem.addClickHandler(new ExecuteFunction());
        MenuItem rwaItem = new MenuItem(CUSTOM_RWA);
        rwaItem.addClickHandler(new RwaFunction());
        resultsItem = new MenuItem(EXPORT_RESULTS);
        resultsItem.setEnabled(false);
        resultsItem.addClickHandler(new ExportFunction());
        traceItem = new MenuItem(TRACE);
        traceItem.setEnabled(false);
        traceItem.addClickHandler(new TraceFunction());
        
        menu.setItems(executeItem, resultsItem, traceItem, rwaItem);
        
        ToolStripMenuButton menuButton = new ToolStripMenuButton(SIMULATION, menu);
        menuButton.setWidth(100);
        return menuButton;
    }
    
    /**
     * @return statistics menu
     */
    private ToolStripMenuButton getStatisticsMenu() {
        Menu menu = new Menu();
        MenuItem statisticsItem = new MenuItem(GENERATE_ESTATISTICS);
        statisticsItem.addClickHandler(new StatisticsFunction());
        
        MenuItem tableItem = new MenuItem(TABLE);
        tableItem.addClickHandler(new TableFunction());
        MenuItem graphsItem = new MenuItem(GRAPHS);
        graphsItem.addClickHandler(new GraphsFunction());
        
        menu.setItems(statisticsItem, tableItem, graphsItem);
        
        ToolStripMenuButton menuButton = new ToolStripMenuButton(ESTATISTICS, menu);
        menuButton.setWidth(100);
        return menuButton;
    }
}
