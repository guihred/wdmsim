package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;
import br.unb.cic.wdm.shared.domain.GroomingType;
import br.unb.cic.wdm.shared.domain.Node;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Function that allows editing the selected node
 * int hte network
 * 
 * @author Guilherme
 * 
 */
public class EditNodeFunction extends FunctionCommand {
    
    /**
     * Field that displays the grooming in of the
     * node
     */
    private final TextBox groomingIn  = new TextBox();
    /**
     * Field that displays the grooming out of the
     * node
     */
    private final TextBox groomingOut = new TextBox();
    /**
     * Selected node in the network
     */
    private Node          selectedNode;
    
    /**
     * @return dialog box with the nodes
     *         information
     */
    private DialogBox createDialogBox() {
        dialogBox = new DialogBox();
        dialogBox.setText(NODE_CONFIG);
        FlexTable dialogContents = new FlexTable();
        TextBox id = new TextBox();
        id.setReadOnly(true);
        id.setMaxLength(6);
        id.setText(Integer.toString(selectedNode.getId()));
        dialogContents.setWidget(0, 0, new Label(ID));
        dialogContents.setWidget(0, 1, id);
        
        dialogContents.setWidget(1, 0, new Label(GROOMING));
        
        final RadioButton fullGrooming = new RadioButton(GROOMING, GroomingType.FULL_GROOMING.getName());
        fullGrooming.setValue(selectedNode.getGroomingType().equals(GroomingType.FULL_GROOMING));
        fullGrooming.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                groomingIn.setEnabled(true);
                groomingOut.setEnabled(true);
                if (!groomingIn.getValue().equals(groomingOut.getValue())) {
                    groomingOut.setValue(groomingIn.getValue());
                    selectedNode.setGroomingOut(selectedNode.getGroomingIn());
                }
            }
        });
        dialogContents.setWidget(2, 0, fullGrooming);
        
        final RadioButton partialGrooming = new RadioButton(GROOMING, GroomingType.PARTIAL_GROOMING.getName());
        partialGrooming.setValue(selectedNode.getGroomingType().equals(GroomingType.PARTIAL_GROOMING));
        partialGrooming.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                groomingIn.setEnabled(true);
                groomingOut.setEnabled(true);
            }
        });
        dialogContents.setWidget(2, 1, partialGrooming);
        
        final RadioButton noGrooming = new RadioButton(GROOMING, GroomingType.NO_GROOMING.getName());
        noGrooming.setValue(selectedNode.getGroomingType().equals(GroomingType.NO_GROOMING));
        noGrooming.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                groomingIn.setEnabled(false);
                groomingOut.setEnabled(false);
                groomingIn.setValue("0");
                groomingOut.setValue("0");
                selectedNode.setGroomingOut(0);
                selectedNode.setGroomingIn(0);
            }
        });
        dialogContents.setWidget(2, 2, noGrooming);
        dialogContents.getFlexCellFormatter().setColSpan(2, 2, 2);
        
        groomingIn.setMaxLength(6);
        groomingIn.setText(selectedNode.getGroomingIn().toString());
        groomingIn.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                selectedNode.setGroomingIn(number.intValue());
                GroomingType groomingType = selectedNode.getGroomingType();
                switch (groomingType) {
                    case FULL_GROOMING:
                        fullGrooming.setValue(true);
                        break;
                    case NO_GROOMING:
                        noGrooming.setValue(true);
                        break;
                    case PARTIAL_GROOMING:
                        partialGrooming.setValue(true);
                        break;
                    default:
                        break;
                }
                
            }
        });
        dialogContents.setWidget(3, 0, new Label(GROOMING_IN_PORTS));
        dialogContents.setWidget(3, 1, groomingIn);
        
        groomingOut.setMaxLength(6);
        groomingOut.setText(selectedNode.getGroomingOut().toString());
        groomingOut.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                selectedNode.setGroomingOut(number.intValue());
                GroomingType groomingType = selectedNode.getGroomingType();
                switch (groomingType) {
                    case FULL_GROOMING:
                        fullGrooming.setValue(true);
                        break;
                    case NO_GROOMING:
                        noGrooming.setValue(true);
                        break;
                    case PARTIAL_GROOMING:
                        partialGrooming.setValue(true);
                        break;
                    default:
                        break;
                }
            }
        });
        dialogContents.setWidget(4, 0, new Label(GROOMING_OUT_PORTS));
        dialogContents.setWidget(4, 1, groomingOut);
        
        final TextBox wlConverters = new TextBox();
        wlConverters.setMaxLength(6);
        wlConverters.setText(selectedNode.getWlConverter().toString());
        wlConverters.addChangeHandler(new ValueChangeFormatter() {
            
            @Override
            public void setValue(Number number) {
                selectedNode.setWlConverter(number.intValue());
            }
        });
        dialogContents.setWidget(5, 0, new Label(WL_CONVERTERS));
        dialogContents.setWidget(5, 1, wlConverters);
        
        Label wlcLabel = new Label(WL_CONVERSION_RANGE);
        wlcLabel.setTitle(WL_CONVERSION_RANGE_HINT);
        final TextBox conversionRange = new TextBox();
        conversionRange.setMaxLength(2);
        conversionRange.setText(selectedNode.getConversionRange().toString());
        conversionRange.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                selectedNode.setConversionRange(number.intValue());
            }
        });
        dialogContents.setWidget(6, 0, wlcLabel);
        dialogContents.setWidget(6, 1, conversionRange);
        
        Button alterButton = new Button(ALTER, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialogBox.hide();
            }
        });
        dialogContents.setWidget(7, 1, alterButton);
        Button alterLinksButton = new Button(ALTER_LINKS, new EditLinksFunction());
        dialogContents.setWidget(7, 2, alterLinksButton);
        dialogBox.setWidget(dialogContents);
        setStyle(dialogBox.getElement().getStyle());
        // Return the dialog box
        dialogBox.addCloseHandler(new CloseHandler<PopupPanel>() {
            @Override
            public void onClose(CloseEvent<PopupPanel> event) {
                mainNetwork.updateNodes();
            }
        });
        
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        selectedNode = mainNetwork.getSelectedNode();
        if (selectedNode != null) {
            
            final DialogBox dialogBox = createDialogBox();
            dialogBox.setModal(true);
            dialogBox.setAnimationEnabled(true);
            dialogBox.setAutoHideEnabled(true);
            dialogBox.setTitle(EDIT_NODE);
            dialogBox.center();
            dialogBox.show();
        }
    }
}
