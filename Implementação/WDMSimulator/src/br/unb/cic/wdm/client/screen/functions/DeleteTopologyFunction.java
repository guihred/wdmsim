package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_DELETE;
import br.unb.cic.wdm.client.screen.MainEditionPanel;

import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;

/**
 * Function that deletes the network topology
 * 
 * @author Guilherme
 * 
 */
public class DeleteTopologyFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.ask(QUESTION_DELETE, new BooleanCallback() {
            @Override
            public void execute(Boolean value) {
                if (value) {
                    mainNetwork.removeAllNode();
                    MainEditionPanel.getInstance().updateCalls();
                }
            }
        }); // XMLInputFactory factory =
            // XMLInputFactory.newInstance();
    }
    
}
