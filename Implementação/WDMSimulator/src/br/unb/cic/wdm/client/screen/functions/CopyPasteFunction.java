package br.unb.cic.wdm.client.screen.functions;

import java.util.ArrayList;
import java.util.List;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

/**
 * Function responsible for copying and pasting
 * node of the simulation
 * 
 * @author Guilherme
 */
public class CopyPasteFunction extends FunctionCommand {
    
    /**
     * List of selected nodes
     */
    private List<Integer> selectedNodesIds;
    
    @Override
    public void execute() {
        selectedNodesIds = mainNetwork.getSelectedNodesIds();
        
        if (selectedNodesIds != null && !selectedNodesIds.isEmpty()) {
            ArrayList<Link> linksToCopy = new ArrayList<Link>();
            ArrayList<Node> nodesToCopy = new ArrayList<Node>();
            for (int i = 0; i < selectedNodesIds.size(); i++) {
                nodesToCopy.add(mainNetwork.addNode());
            }
            
            for (int i = 0; i < selectedNodesIds.size(); i++) {
                Integer id = selectedNodesIds.get(i);
                List<Link> linksById = mainNetwork.getLinksById(id);
                for (Link link : linksById) {
                    if (link.getFrom() == id.intValue() && selectedNodesIds.contains(link.getTo())) {
                        if (!linksToCopy.contains(link)) {
                            linksToCopy.add(link);
                        }
                    }
                    if (link.getTo() == id.intValue() && selectedNodesIds.contains(link.getFrom())) {
                        if (!linksToCopy.contains(link)) {
                            linksToCopy.add(link);
                        }
                    }
                }
                
            }
            for (Link link : linksToCopy) {
                for (int i = 0; i < selectedNodesIds.size(); i++) {
                    for (int j = 0; j < selectedNodesIds.size(); j++) {
                        if (link.getTo() == selectedNodesIds.get(i) && link.getFrom() == selectedNodesIds.get(j)) {
                            int id = nodesToCopy.get(i).getId();
                            int id2 = nodesToCopy.get(j).getId();
                            mainNetwork.addLink(new Link(id, id2));
                            mainNetwork.addLink(new Link(id2, id));
                        }
                    }
                }
            }
        }
        
    }
}
