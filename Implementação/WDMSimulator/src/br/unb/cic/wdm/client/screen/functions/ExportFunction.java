package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DialogBox;

/**
 * Function responsible for exporting the results
 * of the simulation
 * 
 * @author Guilherme
 */
public class ExportFunction extends FunctionCommand {
    /**
     * @return dialog box with the link for saving
     *         the results of the simulation
     */
    private DialogBox createDialogBox() {
        dialogBox = new DialogBox();
        dialogBox.setText(SIMULATION_RESULTS);
        
        final Anchor anchor = new Anchor("Baixar Resultados");
        String moduleBaseURL = GWT.getModuleBaseURL();
        String url = moduleBaseURL.replace("wdmsimulator/", "") + "results?chave=" + mainNetwork.getIdSimulation();
        anchor.setHref(url);
        
        dialogBox.add(anchor);
        setStyle(dialogBox.getElement().getStyle());
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(EXPORT_RESULTS);
        dialogBox.center();
        
        dialogBox.show();
    }
}
