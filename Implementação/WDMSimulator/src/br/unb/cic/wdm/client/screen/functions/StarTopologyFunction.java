package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_NODE;

import java.util.ArrayList;
import java.util.List;

import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;

import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;

/**
 * Creates a star topology
 * 
 * @author Guilherme
 * 
 */
public class StarTopologyFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.askforValue(QUESTION_NODE, new ValueCallback() {
            @Override
            public void execute(String value) {
                int nNodes = Integer.parseInt(value);
                List<Node> nodes = new ArrayList<Node>();
                for (int i = 0; i < nNodes; i++) {
                    nodes.add(mainNetwork.addNode());
                }
                for (int i = 1; i < nNodes; i++) {
                    mainNetwork.addLink(new Link(nodes.get(i).getId(), nodes.get(0).getId()));
                    mainNetwork.addLink(new Link(nodes.get(0).getId(), nodes.get(i).getId()));
                    
                }
            }
        });
        // XMLInputFactory factory =
        // XMLInputFactory.newInstance();
    }
    
}
