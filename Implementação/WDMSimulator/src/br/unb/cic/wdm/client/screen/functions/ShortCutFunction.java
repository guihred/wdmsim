package br.unb.cic.wdm.client.screen.functions;


/**
 * Function that defines the shortcuts action in
 * the keyboard
 * 
 * @author Guilherme
 */
public class ShortCutFunction extends FunctionCommand {
    
    /**
     * Letter A
     */
    private final static int LETTER_A      = 65;
    /**
     * Letter C
     */
    private final static int LETTER_C      = 67;
    /**
     * Key DELETE
     */
    
    private final static int LETTER_DELETE = 46;
    /**
     * Letter E
     */
    private final static int LETTER_E      = 69;
    /**
     * Letter L
     */
    private final static int LETTER_L      = 76;
    
    @Override
    public void execute() {
        int charCode = event.getNativeKeyCode();
        boolean controlKeyDown = event.isControlKeyDown();
        boolean altKeyDown = event.isAltKeyDown();
        if (charCode == LETTER_DELETE) {
            mainNetwork.removeNode();
        }
        if (controlKeyDown && altKeyDown) {
            if (charCode == LETTER_A) {
                mainNetwork.addNode();
            }
            if (charCode == LETTER_C) {
                new CopyPasteFunction().execute();
            }
            if (charCode == LETTER_E) {
                new EditNodeFunction().execute();
            }
            if (charCode == LETTER_L) {
                mainNetwork.addLink();
            }
        }
        
    }
}
