package br.unb.cic.wdm.client.screen.functions;

import br.unb.cic.wdm.client.screen.MainMenu;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Classe que formata para números o conteúdo de
 * campos de texto
 * 
 * @author Guilherme
 * 
 */
public abstract class ValueChangeFormatter implements ChangeHandler {
    
    @Override
    public void onChange(ChangeEvent event) {
        TextBox source = (TextBox) event.getSource();
        String number = source.getText().replaceAll("[^\\d.]", "");
        source.setText(number);
        MainMenu.disableItems();
        if (!number.isEmpty())
            setValue(new Double(number));
    }
    
    /**
     * Método de callback para quando determinado
     * campo número for alterado
     * 
     * @param number
     */
    public abstract void setValue(Number number);
}
