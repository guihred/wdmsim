package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.Date;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.shared.LoginResourceAsync;
import br.unb.cic.wdm.shared.domain.UserData;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

/**
 * @author Guilherme
 * 
 */
public class LoginFunction extends FunctionCommand {
    
    /**
     * Session Duration in Milliseconds
     */
    final long                       DURATION   = 1000 * 60 * 1;                // One
                                                                                 // minute
    /**
     * Class de chamada assíncrona ao recurso REST
     * de Login
     */
    private final LoginResourceAsync loginAsync = LoginResourceAsync.Util.get();
    
    /**
     * Cria a caixa de diálogo para efetuar o
     * login
     * 
     * @return caixa de diálogo criada
     */
    private DialogBox createDialogBox() {
        // Create a dialog box and set the caption
        // text
        final DialogBox dialogBox = new DialogBox();
        dialogBox.setText(LOGIN);
        // dialogBox.
        // Create a table to layout the content
        final FlexTable dialogContents = new FlexTable();
        dialogContents.setWidget(0, 0, new Label(USER));
        
        final TextBox user = new TextBox();
        user.setMaxLength(6);
        dialogContents.setWidget(0, 1, user);
        
        final PasswordTextBox pass = new PasswordTextBox();
        pass.setMaxLength(6);
        dialogContents.setWidget(1, 0, new Label(PASSWORD));
        dialogContents.setWidget(1, 1, pass);
        
        Button openButton = new Button(LOGIN, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                UserData userDTO = new UserData();
                userDTO.setLogin(user.getText());
                userDTO.setPassword(pass.getText());
                
                loginAsync.loginServer(userDTO, new MethodCallback<UserData>() {
                    @Override
                    public void onFailure(Method method, Throwable caught) {
                        Window.alert("Access Denied. Check your user-name and password.");
                    }
                    
                    @Override
                    public void onSuccess(Method m, UserData result) {
                        if (result != null && result.isLoggedIn()) {
                            String sessionID = result.getSessionId();
                            Date expires = new Date(System.currentTimeMillis() + DURATION);
                            Cookies.setCookie("sid", sessionID, expires, null, "/", false);
                        } else {
                            // SC.say("Access Denied. Check your user-name and password.");
                        }
                        
                    }
                    
                });
            }
            
        });
        dialogContents.setWidget(6, 1, openButton);
        dialogBox.setWidget(dialogContents);
        setStyle(dialogBox.getElement().getStyle());
        // Return the dialog box
        return dialogBox;
    }
    
    /**
     * Mostra a caixa de diálogo
     */
    private void displayLoginWindow() {
        dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(LOGIN);
        dialogBox.center();
        dialogBox.show();
        
    }
    
    @Override
    public void execute() {
        
        if (Cookies.getCookie("sid") == null) {
            
            displayLoginWindow();
            ;
        } else {
            loginAsync.loginFromSessionServer(new MethodCallback<UserData>() {
                @Override
                public void onFailure(Method m, Throwable caught) {
                    displayLoginWindow();
                }
                
                @Override
                public void onSuccess(Method m, UserData result) {
                    if (result == null) {
                        dialogBox.hide();
                    } else {
                        if (result.isLoggedIn()) {
                            dialogBox.hide();
                        } else {
                            displayLoginWindow();
                        }
                    }
                }
                
            });
        }
        
    }
}
