package br.unb.cic.wdm.client.elements;

import static br.unb.cic.wdm.client.screen.LabelConstants.SELECT_TWO_NODES;

import java.util.ArrayList;
import java.util.List;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.client.screen.MainBoard;
import br.unb.cic.wdm.client.screen.MainEditionPanel;
import br.unb.cic.wdm.client.screen.functions.EditNodeFunction;
import br.unb.cic.wdm.client.screen.functions.ShortCutFunction;
import br.unb.cic.wdm.shared.FileResourceAsync;
import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;
import br.unb.cic.wdm.shared.domain.Simulation;

import com.chap.links.client.Network;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.Selection;
import com.smartgwt.client.util.SC;

/**
 * Class that contains most basic functions of
 * network modeling modification
 * 
 * @author Guilherme
 * 
 */
public class NetworkUtils implements Runnable {
    
    /**
     * Unique reference to the class
     */
    private static NetworkUtils mainNetwork;
    
    /**
     * @return main network reference
     */
    public static NetworkUtils getMainNetwork() {
        if (mainNetwork == null) {
            mainNetwork = new NetworkUtils();
        }
        return mainNetwork;
    }
    
    /**
     * Identification for the current simulation
     */
    private Long                  idSimulation;
    /**
     * Object responsible for generating the
     * visualization of the network in screen
     */
    private Network               network;
    /**
     * Options for the creation of the network
     */
    private final Network.Options options;
    /**
     * Parameter of weight link visualization of
     * the network
     */
    private boolean               showWeight;
    
    /**
     * Entity of the current simulation
     */
    private final Simulation      simulation = new Simulation();
    
    /**
     * Constructor
     */
    private NetworkUtils() {
        options = Network.Options.create();
        options.setWidth(MainBoard.getNetworkWidth() + "px");
        options.setHeight(MainBoard.getNetworkHeight() + "px");
    }
    
    /**
     * Adds a link between two selected nodes
     */
    public void addLink() {
        JsArray<Selection> selections = network.getSelections();
        
        int length = selections.length();
        if (length == 2) {
            Selection selection1 = selections.get(0);
            Selection selection2 = selections.get(1);
            if (selection1.isRow() && selection2.isRow()) {
                int id1 = getIdByRow(selection1.getRow());
                int id2 = getIdByRow(selection2.getRow());
                Link link = new Link(id1, id2);
                simulation.getLinks().add(link);
                Link link2 = new Link(id2, id1);
                simulation.getLinks().add(link2);
                network.addLinks(asDataTable(link));
                network.addLinks(asDataTable(link2));
                updateLinks();
            }
        } else {
            SC.say(SELECT_TWO_NODES);
        }
        
    }
    
    /**
     * Adds a link to the network
     * 
     * @param link
     */
    public void addLink(Link link) {
        simulation.getLinks().add(link);
        network.addLinks(asDataTable(link));
        updateLinks();
    }
    
    /**
     * Adds a node to the network
     * 
     * @return added node
     */
    public Node addNode() {
        Node node = new Node(simulation.getNodes().size());
        simulation.getNodes().add(node);
        getNetwork().addNodes(asDataTable(node));
        return node;
    }
    
    /**
     * Converts the link into a DataTable, class
     * that contains logic in javascript for
     * rendering the network
     * 
     * @param link
     * @return dataTable
     */
    public DataTable asDataTable(Link link) {
        DataTable links = DataTable.create();
        links.addColumn(ColumnType.NUMBER, "from");
        links.addColumn(ColumnType.NUMBER, "to");
        links.addRow();
        links.setValue(0, 0, link.getFrom());
        links.setValue(0, 1, link.getTo());
        return links;
    }
    
    /**
     * Converts the node into a DataTable, class
     * that contains logic in javascript for
     * rendering the network
     * 
     * @param node
     * @return dataTable
     */
    public DataTable asDataTable(Node node) {
        DataTable nodeRow = DataTable.create();
        nodeRow.addColumn(ColumnType.NUMBER, "id");
        nodeRow.addColumn(ColumnType.STRING, "text");
        nodeRow.addColumn(ColumnType.STRING, "action");
        
        nodeRow.addRow();
        nodeRow.setValue(0, 0, node.getId());
        nodeRow.setValue(0, 1, node.getText());
        nodeRow.setValue(0, 2, node.isActive() ? "create" : "delete");
        return nodeRow;
    }
    
    /**
     * Converts a DataTable row into a node's id
     * 
     * @param row
     * @return node's id
     */
    private int getIdByRow(int row) {
        List<Node> nodesAux = new ArrayList<Node>();
        nodesAux.addAll(simulation.getNodes());
        for (int i = 0; i < nodesAux.size(); i++) {
            if (!nodesAux.get(i).isActive()) {
                nodesAux.remove(i);
                i--;
            }
        }
        int result = nodesAux.get(row).getId();
        
        return result;
    }
    
    public Long getIdSimulation() {
        return idSimulation;
    }
    
    /**
     * @param id
     *            of the node
     * @return links that contain the node id
     */
    public List<Link> getLinksById(int id) {
        List<Link> linksById = new ArrayList<Link>();
        
        for (int i = 0; i < simulation.getLinks().size(); i++) {
            Link link = simulation.getLinks().get(i);
            if (link.getFrom() == id || link.getTo() == id) {
                linksById.add(link);
            }
        }
        
        return linksById;
    }
    
    /**
     * @return class of visualization of the
     *         network
     */
    public Network getNetwork() {
        return network;
    }
    
    /**
     * @param id
     * @return node for the given id
     */
    public Node getNodeById(int id) {
        for (int i = 0; i < simulation.getNodes().size(); i++) {
            if (simulation.getNodes().get(i).getId() == id) {
                return simulation.getNodes().get(i);
            }
        }
        return null;
    }
    
    /**
     * Returns the current selected node. Returns
     * null if there are no selected nodes or many
     * selected nodes
     * 
     * @return selected node
     */
    public Node getSelectedNode() {
        JsArray<Selection> selections = network.getSelections();
        
        int length = selections.length();
        if (length == 1) {
            Selection selection1 = selections.get(0);
            if (selection1.isRow()) {
                int id = getIdByRow(selection1.getRow());
                Node node = getNodeById(id);
                return node;
            }
        }
        return null;
    }
    
    /**
     * @return list of selected node's ids
     */
    public List<Integer> getSelectedNodesIds() {
        JsArray<Selection> selections = network.getSelections();
        
        int length = selections.length();
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for (int j = 0; j < length; j++) {
            Selection selection1 = selections.get(j);
            if (selection1.isRow()) {
                ids.add(getIdByRow(selection1.getRow()));
            }
        }
        return ids;
    }
    
    /**
     * @return simulation
     */
    public Simulation getSimulation() {
        return simulation;
    }
    
    /**
     * Removes all node contained in the network
     */
    public void removeAllNode() {
        
        DataTable linkData = DataTable.create();
        linkData.addColumn(ColumnType.NUMBER, "from");
        linkData.addColumn(ColumnType.NUMBER, "to");
        
        network.setLinks(linkData);
        DataTable nodeData = DataTable.create();
        nodeData.addColumn(ColumnType.NUMBER, "from");
        nodeData.addColumn(ColumnType.NUMBER, "to");
        network.setNodes(nodeData);
        network.redraw();
        simulation.getNodes().clear();
        simulation.getLinks().clear();
        simulation.getCalls().clear();
        
    }
    
    /**
     * Remove selected node
     */
    public void removeNode() {
        List<Integer> ids = getSelectedNodesIds();
        for (int j = 0; j < ids.size(); j++) {
            for (int i = 0; i < simulation.getLinks().size(); i++) {
                Link link = simulation.getLinks().get(i);
                if (link.getFrom() == ids.get(j) || link.getTo() == ids.get(j)) {
                    simulation.getLinks().remove(i);
                    i--;
                }
            }
            Node nodePorId = getNodeById(ids.get(j));
            nodePorId.setActive(false);
            network.addNodes(asDataTable(nodePorId));
        }
        updateLinks();
        
    }
    
    /**
     * Initialization function of the network
     * 
     */
    @Override
    public void run() {
        
        DataTable node = DataTable.create();
        node.addColumn(ColumnType.NUMBER, "id");
        node.addColumn(ColumnType.STRING, "text");
        
        DataTable links = DataTable.create();
        links.addColumn(ColumnType.NUMBER, "from");
        links.addColumn(ColumnType.NUMBER, "to");
        
        network = new Network(node, links, options);
        network.addSelectHandler(MainEditionPanel.getInstance());
        
        RootPanel.get().addDomHandler(new ShortCutFunction(), KeyDownEvent.getType());
        RootPanel.get().addDomHandler(new EditNodeFunction(), DoubleClickEvent.getType());
        
        RootPanel principal = RootPanel.get("principal");
        principal.getElement().getStyle().setPosition(Position.RELATIVE);
        
        principal.add(network, 21, 21);
        DOM.removeChild(RootPanel.getBodyElement(), DOM.getElementById("loading"));
        String token = History.getToken();
        if (token != null && !token.isEmpty()) {
            FileResourceAsync.Util.get().openSimulation(Long.valueOf(token), new MethodCallback<Simulation>() {
                
                @Override
                public void onFailure(Method method, Throwable exception) {
                    SC.say(exception.getLocalizedMessage());
                }
                
                @Override
                public void onSuccess(Method method, Simulation result) {
                    if (result != null) {
                        mainNetwork.setSimulation(result);
                    }
                }
            });
        }
    }
    
    /**
     * @param idSimulation
     */
    public void setIdSimulation(Long idSimulation) {
        this.idSimulation = idSimulation;
    }
    
    /**
     * @param showWeight
     */
    public void setShowWeight(boolean showWeight) {
        this.showWeight = showWeight;
    }
    
    /**
     * @param result
     */
    public void setSimulation(Simulation result) {
        
        removeAllNode();
        this.simulation.setCalls(result.getCalls());
        simulation.getNodes().addAll(result.getNodes());
        simulation.getLinks().addAll(result.getLinks());
        updateNodes();
        updateLinks();
        MainEditionPanel.getInstance().updateCalls();
    }
    
    /**
     * Update the state of links in screen
     */
    public void updateLinks() {
        int linksSize = simulation.getLinks().size();
        
        Double max = 0d;
        
        if (linksSize < 50) {
            for (Link li : simulation.getLinks()) {
                max = (max > li.getDelay()) ? max : li.getDelay();
                
            }
        }
        
        DataTable linkData = DataTable.create();
        linkData.addColumn(ColumnType.NUMBER, "from");
        linkData.addColumn(ColumnType.NUMBER, "to");
        linkData.addColumn(ColumnType.STRING, "text");
        
        if (linksSize < 50)
            linkData.addColumn(ColumnType.NUMBER, "length");
        
        for (int i = 0; i < linksSize; i++) {
            Link link = simulation.getLinks().get(i);
            linkData.addRow();
            linkData.setValue(i, 0, link.getFrom());
            linkData.setValue(i, 1, link.getTo());
            double value = ((Double) (link.getDelay().doubleValue() / max.doubleValue() * 100 + 20)).doubleValue();
            if (showWeight)
                linkData.setValue(i, 2, link.getWeight().toString());
            if (linksSize < 50)
                linkData.setValue(i, 3, value);
            
        }
        network.setLinks(linkData);
        network.redraw();
    }
    
    /**
     * Updates the state of nodes in screen
     */
    public void updateNodes() {
        DataTable nodeData = DataTable.create();
        nodeData.addColumn(ColumnType.NUMBER, "id");
        nodeData.addColumn(ColumnType.STRING, "text");
        nodeData.addColumn(ColumnType.STRING, "action");
        nodeData.addColumn(ColumnType.STRING, "backgroundColor");
        for (int i = 0; i < simulation.getNodes().size(); i++) {
            Node node = simulation.getNodes().get(i);
            if (node.isActive()) {
                nodeData.addRow();
                nodeData.setValue(i, 0, node.getId());
                nodeData.setValue(i, 1, node.getText());
                nodeData.setValue(i, 2, "update");
                nodeData.setValue(i, 3, node.getGroomingType().getColor());
                
            }
        }
        network.addNodes(nodeData);
        network.redraw();
    }
    // private void updatePackages() {
    // Timer timer = null;
    // try {
    // timer = new Timer() {
    // private DataTable packagesTable =
    // DataTable.create();
    // private int timeStamp = 1;
    //
    // private ArrayList<Package>
    // getPackagesByTimestamp() {
    // ArrayList<Package> arrayList = new
    // ArrayList<Package>();
    // for (int i = 0; i < packages.size(); i++) {
    // if (packages.get(i).getTimestamp() ==
    // timeStamp) {
    // arrayList.add(packages.get(i));
    // }
    // }
    // return arrayList;
    // }
    //
    // private void newPackagesTable() {
    // packagesTable = DataTable.create();
    // packagesTable.addColumn(ColumnType.NUMBER,
    // "id");
    // packagesTable.addColumn(ColumnType.NUMBER,
    // "from");
    // packagesTable.addColumn(ColumnType.NUMBER,
    // "to");
    // packagesTable.addColumn(ColumnType.STRING,
    // "color");
    // packagesTable.addColumn(ColumnType.NUMBER,
    // "duration");
    // packagesTable.addColumn(ColumnType.STRING,
    // "action");
    //
    // }
    //
    // @Override
    // public void run() {
    // ArrayList<Package> packagesByTimestamp =
    // getPackagesByTimestamp();
    // if (packagesByTimestamp.isEmpty() &&
    // timeStamp > Node.nMensagens) {
    // this.cancel();
    // } else {
    // newPackagesTable();
    // for (int i = 0; i <
    // packagesByTimestamp.size(); i++) {
    // Package package1 =
    // packagesByTimestamp.get(i);
    // packagesTable.addRow();
    // packagesTable.setValue(i, 0,
    // package1.getId());// id
    // packagesTable.setValue(i, 1,
    // package1.getFrom());// from
    // packagesTable.setValue(i, 2,
    // package1.getTo());// to
    // packagesTable.setValue(i, 3,
    // package1.getTipo().getColor());// color
    // packagesTable.setValue(i, 4, 0.5);//
    // duration
    // packagesTable.setValue(i, 5, "create");//
    // action
    // }
    // addPackages(packagesTable);
    // timeStamp++;
    // }
    //
    // }
    //
    // };
    // timer.scheduleRepeating(500);
    //
    // } catch (JavaScriptException e) {
    // timer.cancel();
    // SC.say("Houve um erro de packages");
    // } catch (Exception e) {
    // timer.cancel();
    // }
    //
    // }
    
}