package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.QUESTION_NEW_SIMULATION;
import br.unb.cic.wdm.client.screen.MainEditionPanel;

import com.google.gwt.user.client.History;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;

/**
 * Creates a new simulation
 * 
 * @author Guilherme
 */
public class NewSimulationFunction extends FunctionCommand {
    
    @Override
    public void execute() {
        SC.ask(QUESTION_NEW_SIMULATION, new BooleanCallback() {
            @Override
            public void execute(Boolean value) {
                if (value) {
                    mainNetwork.removeAllNode();
                    MainEditionPanel.getInstance().updateCalls();
                    History.newItem("");
                }
            }
        });
    }
}
