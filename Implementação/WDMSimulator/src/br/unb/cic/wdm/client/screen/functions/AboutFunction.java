package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.ABOUT;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;

/**
 * Function responsible for showing information
 * about the application
 * 
 * @author Guilherme
 */
public class AboutFunction extends FunctionCommand {
    
    /**
     * @return caixa de diálog com as informações
     */
    private DialogBox createDialogBox() {
        
        dialogBox = new DialogBox();
        dialogBox.setText(ABOUT);
        final FlexTable table = new FlexTable();
        table.setWidget(0, 0, new Label("WDMSim"));
        table.setWidget(1, 0, new Label("Este Simulador foi criado por"));
        table.setWidget(2, 0, new Label("Autores: André Drummond e Guilherme Fernandes"));
        table.setWidget(3, 0, new Label("Email: andred@cic.unb.br, guih.red@gmail.com"));
        table.setWidget(4, 0, new Label("Versão 1.0"));
        table.setWidget(5, 0, new Label("2014"));
        
        dialogBox.setWidget(table);
        
        setStyle(dialogBox.getElement().getStyle());
        // Return the dialog box
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        final DialogBox dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(ABOUT);
        dialogBox.center();
        dialogBox.show();
    }
}
