package br.unb.cic.wdm.client.screen;

/**
 * Class with constants of labels used in the
 * application
 * 
 * @author Guilherme
 * 
 */
@SuppressWarnings("javadoc")
public final class LabelConstants {
    public static final String ABOUT                    = "Sobre..";
    public static final String ADD                      = "Adicionar";
    public static final String ALTER                    = "Alterar";
    public static final String ALTER_LINKS              = "Alterar Links";
    public static final String BANDWIDTH                = "Largura de Banda";
    public static final String CALLS                    = "Chamadas";
    public static final String CALLS_INFORMATION        = "Informações de Chamadas";
    public static final String CALLS_NUMBER             = "Número de Chamadas";
    public static final String CALLS_WARNING            = "É necessário cadastrar ao menos uma chamada";
    public static final String CHOOSE                   = "Escolha";
    public static final String CHOOSE_FILE              = "Escolha o Arquivo";
    public static final String COPY_PASTE               = "Copiar/Colar";
    public static final String COS                      = "COS";
    public static final String COS_HINT                 = "Classe de Serviço";
    public static final String CREATE_TOPOLOGY          = "Criar Topologia";
    public static final String CUSTOM_RWA               = "RWA Personalizado";
    public static final String DELAY                    = "Atraso";
    public static final String DELETE                   = "Deletar";
    public static final String EDIT                     = "Editar";
    public static final String EDIT_NODE                = "Editar Nó";
    public static final String ESTATISTICS              = "Estatísticas";
    public static final String EXECUTE                  = "Executar";
    public static final String EXECUTE_SIMULATION       = "Executar Simulação";
    public static final String EXECUTE_WARNING          = "É preciso executar a simulação.";
    public static final String EXECUTING_SIMULATION     = "Executando Simulação";
    public static final String EXPORT_RESULTS           = "Exportar Resultados";
    public static final String EXPORT_TRACE             = "Exportar Trace";
    public static final String FILE                     = "Arquivo";
    public static final String FROM                     = "De";
    public static final String FULL                     = "Completa";
    public static final String GENERATE_ESTATISTICS     = "Gerar Estatísticas";
    public static final String GRAPH_CALLS              = "Number of Calls by Pair";
    public static final String GRAPHS                   = "Gráficos";
    public static final String GROOMING                 = "Agregação";
    public static final String GROOMING_IN_PORTS        = "Agregação de Entrada";
    public static final String GROOMING_OUT_PORTS       = "Agregação de Saída";
    public static final String HELP                     = "Ajuda";
    public static final String HOLDING_TIME             = "TMD";
    public static final String HOLDING_TIME_HINT        = "Tempo Médio de Duração";
    public static final String ID                       = "Id";
    public static final String LINK_CONFIG              = "Configuração dos Links";
    public static final String LINK_INFORMATION         = "Informações de Links";
    public static final String LOAD                     = "Carga";
    public static final String LOGIN                    = "Login";
    public static final String MAX_RATE                 = "Taxa Máxima";
    public static final String MBBR                     = "MBBR";
    public static final String MBBR_HINT                = "Mean Bandwidth Blocking Rate";
    public static final String MBP                      = "MBP";
    public static final String MBP_HINT                 = "Mean Blocking Probability(%)";
    public static final String NEW                      = "Novo";
    public static final String NEW_CALL                 = "Nova Chamada";
    public static final String NEW_LINK                 = "Novo Link";
    public static final String NEW_NODE                 = "Novo Roteador";
    public static final String NEXT                     = "Próximo";
    public static final String NODE_CONFIG              = "Configuração do Nó";
    public static final String NODE_INFORMATION         = "Informações de Nó";
    public static final String OPEN                     = "Abrir";
    public static final String OPEN_CONFIG_FILE         = "Abrir Arquivo de Configuração";
    public static final String OPEN_ERROR               = "É necessário o arquivo de configuração.";
    public static final String PASSWORD                 = "Senha";
    public static final String PREVIOUS                 = "Anterior";
    public static final String QUESTION_DELETE          = "Tem certeza de que quer deletar toda a rede?";
    public static final String QUESTION_NEW_SIMULATION  = "Tem certeza de que quer reiniciar a simulação?";
    public static final String QUESTION_NODE            = "Digite o número de nós";
    public static final String RANDOM                   = "Aleatório";
    public static final String RATE                     = "Taxa";
    public static final String REMOVE_CALL              = "Remover Chamada";
    public static final String REMOVE_NODE              = "Remover Nó";
    public static final String RING                     = "Anel";
    public static final String RWA                      = "RWA";
    public static final String RWA_HINT                 = "Routing and Wavelength Assignment";
    public static final String SAVE                     = "Salvar";
    public static final String SAVE_CONFIG_FILE         = "Salvar Arquivo de Configuração";
    public static final String SAVE_STATS_FILE          = "Salvar Arquivo de Estatísticas";
    public static final String SELECT_TWO_NODES         = "Selecione 2 nós da rede pressionando Ctrl";
    public static final String SHOW_WEIGHT              = "Mostrar Peso";
    public static final String SIMULATION               = "Simulação";
    public static final String SIMULATION_CHANGED       = "Parâmetro de simulação mudou, gostaria de executar a simulação?";
    public static final String SIMULATION_CONFIG        = "Configuração da Simulação";
    public static final String SIMULATION_NOT_RUN       = "É preciso rodar a simulação primeiro.";
    public static final String SIMULATION_RESULTS       = "Resultados da Simulação";
    public static final String SIMULATION_RUNNING       = "Simulação em execução, por favor aguarde.";
    public static final String SIMULATION_SUCCESS       = "Simulação executada com Sucesso!";
    public static final String STAR                     = "Estrela";
    public static final String TABLE                    = "Tabelas";
    public static final String TEXT                     = "Texto";
    public static final String TO                       = "Para";
    public static final String TOPOLOGY_WARNING         = "Topologia de Rede não definida";
    public static final String TRACE                    = "Trace";
    public static final String TUTORIAL                 = "Tutorial";
    public static final String TUTORIAL_MSG_1           = "Crie sua topologia de rede utilizando as ferramentas gráficas.";
    public static final String TUTORIAL_MSG_2           = "Pode-se realizar a exclusão de múltiplos roteadores de rede.";
    public static final String TUTORIAL_MSG_3           = "Ao fim da construção, a simulação pode ser executada.";
    public static final String USER                     = "Usuário";
    public static final String VERSION                  = "Versão";
    public static final String WAVELENGTH               = "Faixas de Onda";
    public static final String WEIGHT                   = "Peso";
    public static final String WEIGHT_CALL              = "Peso";
    public static final String WL_CONVERSION_RANGE      = "Intervalo de Conversão";
    public static final String WL_CONVERSION_RANGE_HINT = "Intervalo de Conversão";
    public static final String WL_CONVERTERS            = "Conversor de Cumprimento";
    
}