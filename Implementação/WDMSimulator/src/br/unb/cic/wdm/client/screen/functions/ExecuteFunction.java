package br.unb.cic.wdm.client.screen.functions;

import static br.unb.cic.wdm.client.screen.LabelConstants.*;

import java.util.List;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import br.unb.cic.wdm.client.screen.MainMenu;
import br.unb.cic.wdm.shared.domain.Simulation;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.smartgwt.client.util.SC;

/**
 * Function responsible for presenting the
 * execution parameters for the simulation
 * 
 * @author Guilherme
 * 
 */
public class ExecuteFunction extends FunctionCommand {
    
    /**
     * Image exhibited while the user waits for
     * the execution of the simulation
     */
    private final Image image1 = new Image("./wdmsimulator/sc/skins/TreeFrog/images/loading.gif");
    
    /**
     * @return dialog box with the parameters of
     *         the simulation
     */
    private DialogBox createDialogBox() {
        final DialogBox dialogBox = new DialogBox();
        dialogBox.setText(SIMULATION_CONFIG);
        final FlexTable dialogContents = new FlexTable();
        dialogContents.setWidget(0, 0, new Label(RWA_HINT));
        dialogContents.getFlexCellFormatter().setColSpan(0, 0, 2);
        
        rwaResourceAsync.getRwaOptions(new MethodCallback<List<String>>() {
            
            @Override
            public void onFailure(Method method, Throwable caught) {
                SC.warn(caught.getLocalizedMessage());
            }
            
            @Override
            public void onSuccess(Method method, List<String> result) {
                final ListBox listBox = new ListBox();
                for (int i = 0; i < result.size(); i++) {
                    
                    final String rwaClass = result.get(i);
                    String name = rwaClass;
                    if (name.contains("/")) {
                        String[] split = name.split("/");
                        name = split[0];
                    }
                    listBox.addItem(name, rwaClass);
                }
                ChangeHandler handler = new ChangeHandler() {
                    @Override
                    public void onChange(ChangeEvent event) {
                        int i = listBox.getSelectedIndex();
                        mainNetwork.getSimulation().setRwaModule(listBox.getValue(i));
                    }
                };
                listBox.addChangeHandler(handler);
                dialogContents.setWidget(1, 0, listBox);
            }
        });
        
        final TextBox calls = new TextBox();
        calls.setText(mainNetwork.getSimulation().getTrafficCalls().toString());
        calls.setMaxLength(6);
        calls.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                mainNetwork.getSimulation().setTrafficCalls(number.intValue());
            }
        });
        
        dialogContents.setWidget(2, 0, new Label(CALLS));
        dialogContents.setWidget(2, 1, calls);
        
        final TextBox load = new TextBox();
        load.setText(mainNetwork.getSimulation().getTrafficLoad().toString());
        load.setMaxLength(6);
        load.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                mainNetwork.getSimulation().setTrafficLoad(number.intValue());
            }
        });
        dialogContents.setWidget(3, 0, new Label(LOAD));
        dialogContents.setWidget(3, 1, load);
        
        final TextBox version = new TextBox();
        version.setText(mainNetwork.getSimulation().getWdmVersion());
        version.setMaxLength(6);
        version.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                mainNetwork.getSimulation().setWdmVersion(number.toString());
            }
        });
        dialogContents.setWidget(4, 0, new Label(VERSION));
        dialogContents.setWidget(4, 1, version);
        
        final TextBox wavelength = new TextBox();
        wavelength.setText(mainNetwork.getSimulation().getWavelengths().toString());
        wavelength.setMaxLength(2);
        wavelength.addChangeHandler(new ValueChangeFormatter() {
            @Override
            public void setValue(Number number) {
                mainNetwork.getSimulation().setWavelengths(number.intValue());
            }
        });
        dialogContents.setWidget(5, 0, new Label(WAVELENGTH));
        dialogContents.setWidget(5, 1, wavelength);
        
        Button executeButton = new Button(EXECUTE, new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                executeSimulation(dialogBox);
            }
            
        });
        dialogContents.setWidget(6, 1, executeButton);
        dialogBox.setWidget(dialogContents);
        setStyle(dialogBox.getElement().getStyle());
        // Return the dialog box
        return dialogBox;
    }
    
    @Override
    public void execute() {
        
        final DialogBox dialogBox = createDialogBox();
        dialogBox.setModal(true);
        dialogBox.setAnimationEnabled(true);
        dialogBox.setAutoHideEnabled(true);
        dialogBox.setTitle(EXECUTE_SIMULATION);
        dialogBox.center();
        
        dialogBox.show();
    }
    
    /**
     * Executes a call for the execution of the
     * simulation
     * 
     * @param dialogBox
     */
    public void executeSimulation(final DialogBox dialogBox) {
        Simulation simulation = mainNetwork.getSimulation();
        //
        
        if (simulation.getCalls().isEmpty()) {
            SC.warn(CALLS_WARNING);
            dialogBox.hide();
        } else if (simulation.getNodes().isEmpty() || simulation.getLinks().isEmpty()) {
            SC.warn(TOPOLOGY_WARNING);
            dialogBox.hide();
        } else {
            final DialogBox dialogBox2 = new DialogBox();
            setStyle(dialogBox2.getElement().getStyle());
            HorizontalPanel horizontalPanel = new HorizontalPanel();
            horizontalPanel.add(image1);
            horizontalPanel.add(new Label(SIMULATION_RUNNING));
            
            dialogBox2.add(horizontalPanel);
            dialogBox2.center();
            dialogBox2.show();
            try {
                resourceAsync.executeSimulation(simulation, new MethodCallback<Long>() {
                    
                    @Override
                    public void onFailure(Method method, Throwable exception) {
                        SC.warn(exception.getLocalizedMessage());
                        
                    }
                    
                    @Override
                    public void onSuccess(Method method, Long result) {
                        mainNetwork.setIdSimulation(result);
                        MainMenu.enableItems();
                        SC.say(SIMULATION_SUCCESS);
                        History.newItem(result.toString());
                        dialogBox2.hide();
                    }
                });
            } catch (Exception e) {
                SC.warn(e.getLocalizedMessage());
            }
            
        }
    }
    
}
