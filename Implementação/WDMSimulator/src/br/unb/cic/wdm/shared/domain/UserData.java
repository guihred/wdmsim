package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Dados de usuário do sistema
 * 
 * @author Guilherme
 * 
 */
@Entity
public class UserData implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Identificador do usuário
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              id;
    
    /**
     * Estado de login do usuário
     */
    @Transient
    private boolean           loggedIn;
    
    /**
     * Login do usuário
     */
    @Column
    private String            login;
    
    /**
     * Hash da Senha do usuário e o login
     */
    private String            password;
    /**
     * Id de Sessão
     */
    @Transient
    private String            sessionId;
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserData other = (UserData) obj;
        if (sessionId == null) {
            if (other.sessionId != null)
                return false;
        } else if (!sessionId.equals(other.sessionId))
            return false;
        return true;
    }
    
    /**
     * @return id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * @return login
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * @return sessionId
     */
    public String getSessionId() {
        return sessionId;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
        return result;
    }
    
    /**
     * @return se o usuário está logado
     */
    public boolean isLoggedIn() {
        return loggedIn;
    }
    
    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @param loggedIn
     */
    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    /**
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }
    
    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @param sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
}
