package br.unb.cic.wdm.shared;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import br.unb.cic.wdm.shared.domain.Statistics;

import com.google.gwt.core.client.GWT;

/**
 * Disponibiliza chamadas remotas a serviços
 * relacionados a estatísticas
 * 
 * @author Guilherme
 * 
 */
public interface StatisticsResourceAsync extends RestService {
    
    /**
     * Utility class to get the instance of the
     * Rest Service
     */
    public static final class Util {
        
        /**
         * Referência única à classe de serviço
         */
        private static StatisticsResourceAsync instance;
        
        /**
         * @return referência a serviços de
         *         estatísticas
         */
        public static final StatisticsResourceAsync get() {
            if (instance == null) {
                instance = GWT.create(StatisticsResourceAsync.class);
                ((RestServiceProxy) instance).setResource(new Resource("/rest/stats"));
            }
            return instance;
        }
        
        /**
         * 
         */
        private Util() {
        }
    }
    
    /**
     * Recupera estatísticas geradas pela
     * simulação
     * 
     * @param idSimulation
     * @param callback
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    void openStatistics(@PathParam("id") Long idSimulation, MethodCallback<List<Statistics>> callback);
    
}
