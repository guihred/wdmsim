package br.unb.cic.wdm.shared;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import br.unb.cic.wdm.shared.domain.Simulation;

import com.google.gwt.core.client.GWT;

/**
 * Permite acesso remoto aos serviços de simulação
 * 
 * @author Guilherme
 * 
 */
public interface SimulationResourceAsync extends RestService {
    
    /**
     * Utility class to get the instance of the
     * Rest Service
     */
    public static final class Util {
        
        /**
         * Intância do serviço
         */
        private static SimulationResourceAsync instance;
        
        /**
         * @return referência ao recurso
         */
        public static final SimulationResourceAsync get() {
            if (instance == null) {
                instance = GWT.create(SimulationResourceAsync.class);
                ((RestServiceProxy) instance).setResource(new Resource("/rest/sim"));
                // System.out.println(((RestServiceProxy)
                // instance).getResource().getPath());
            }
            return instance;
        }
        
        /**
         * 
         */
        private Util() {
        }
    }
    
    /**
     * Cria um id sobre o qual o usuário pode
     * realizar uma submissão de arquivo XML
     * 
     * @param callback
     */
    @GET
    @Path("id")
    void createUploadUrl(MethodCallback<String> callback);
    
    /**
     * Permite ao usuário submeter uma simulação
     * modelada e recuperar o seu id
     * 
     * @param simulation
     * @param callback
     * @throws Exception
     */
    @POST
    void executeSimulation(Simulation simulation, MethodCallback<Long> callback) throws Exception;
    
}
