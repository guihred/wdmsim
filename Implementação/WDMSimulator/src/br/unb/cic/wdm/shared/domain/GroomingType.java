package br.unb.cic.wdm.shared.domain;

/**
 * Classe mostra os tipos de agregação presentes
 * em nós da rede óptica
 * 
 * @author Guilherme
 * 
 */
public enum GroomingType {
    /**
     * Agregação total
     */
    FULL_GROOMING("Total", "#00CCCC"), /**
     * Sem
     * Agregação
     */
    NO_GROOMING("Sem", "#009999"), /**
     * Agregação
     * Parcial
     */
    PARTIAL_GROOMING("Parcial", "#006666");
    
    /**
     * Cor de determinado tipo de agregação
     */
    private final String color;
    /**
     * Nome de determinado tipo de agregação
     */
    private final String name;
    
    /**
     * @param name
     * @param color
     */
    private GroomingType(String name, String color) {
        this.name = name;
        this.color = color;
    }
    
    /**
     * @return color
     */
    public String getColor() {
        return color;
    }
    
    /**
     * @return name
     */
    public String getName() {
        return name;
    }
}
