package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Classe que representa um nó da rede, um
 * elemento óptico com capacidade de roteamento e
 * multiplexação de comprimento de onda
 * 
 * 
 * @author Guilherme
 * 
 */
@Entity(name = "Node")
@Table(name = "NODE")
public class Node implements Comparable<Node>, Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Indicador se determinado nó está ativo na
     * rede ou não
     */
    @Transient
    private boolean           active           = true;
    
    /**
     * Identificador único para determinado nó no
     * banco
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              codigo;
    
    /**
     * Amplitude de conversão do nó
     */
    @Column
    private Integer           conversionRange  = 0;
    
    /**
     * Agregação de entrada
     */
    @Column
    private Integer           groomingIn       = 32;
    
    /**
     * Agregação de saída
     */
    @Column
    private Integer           groomingOut      = 32;
    
    /**
     * Identificador de um nó dentro de uma
     * simulação
     */
    @Column
    private int               id;
    
    /**
     * Texto de um nó
     */
    @Column
    private String            text;
    
    /**
     * Número de conversores de comprimento de
     * onda
     */
    @Column
    private Integer           wlConverter      = 0;
    
    /**
     * 
     */
    public Node() {
    }
    
    /**
     * @param id
     */
    public Node(int id) {
        this.id = id;
        text = "Node " + id;
    }
    
    @Override
    public int compareTo(Node arg0) {
        return arg0.id - this.id;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Node other = (Node) obj;
        if (codigo == null) {
            if (other.codigo != null)
                return false;
        } else if (!codigo.equals(other.codigo))
            return false;
        return true;
    }
    
    /**
     * @return codigo
     */
    public final Long getCodigo() {
        return codigo;
    }
    
    /**
     * @return conversionRange
     */
    public Integer getConversionRange() {
        return conversionRange;
    }
    
    /**
     * @return groomingIn
     */
    public Integer getGroomingIn() {
        return groomingIn;
    }
    
    /**
     * @return groomingOut
     */
    public Integer getGroomingOut() {
        return groomingOut;
    }
    
    /**
     * @return groomingType
     */
    @Transient
    public GroomingType getGroomingType() {
        if (groomingIn.intValue() == 0 && groomingOut.intValue() == 0) {
            return GroomingType.NO_GROOMING;
        } else if (groomingIn.intValue() == groomingOut.intValue()) {
            return GroomingType.FULL_GROOMING;
        } else {
            return GroomingType.PARTIAL_GROOMING;
        }
    }
    
    /**
     * @return id
     */
    public int getId() {
        return id;
    }
    
    /**
     * @return text
     */
    public String getText() {
        return text;
    }
    
    /**
     * @return wlConverter
     */
    public Integer getWlConverter() {
        return wlConverter;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
        return result;
    }
    
    /**
     * @return active
     */
    public boolean isActive() {
        return active;
    }
    
    /**
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    
    /**
     * @param codigo
     */
    public final void setCodigo(Long codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @param conversionRange
     */
    public void setConversionRange(Integer conversionRange) {
        this.conversionRange = conversionRange;
    }
    
    /**
     * @param groomingIn
     */
    public void setGroomingIn(Integer groomingIn) {
        this.groomingIn = groomingIn;
    }
    
    /**
     * @param groomingOut
     */
    public void setGroomingOut(Integer groomingOut) {
        this.groomingOut = groomingOut;
    }
    
    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }
    
    /**
     * @param wlConverter
     */
    public void setWlConverter(Integer wlConverter) {
        this.wlConverter = wlConverter;
    }
    
    @Override
    public String toString() {
        return "Node [ativo=" + active + ", codigo=" + codigo + ", conversionRange=" + conversionRange
                + ", groomingIn=" + groomingIn + ", groomingOut=" + groomingOut + ", id=" + id + ", text=" + text
                + ", wlConverter=" + wlConverter + "]";
    }
    
}
