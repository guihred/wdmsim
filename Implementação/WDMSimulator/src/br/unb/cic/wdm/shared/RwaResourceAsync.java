package br.unb.cic.wdm.shared;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;

/**
 * Interface que permite realizar chamadas a
 * processos remotos e ter acesso a recurso de RWA
 * 
 * 
 * @author Guilherme
 * 
 */
public interface RwaResourceAsync extends RestService {
    
    /**
     * Utility class to get the instance of the
     * Rest Service
     */
    public static final class Util {
        
        /**
         * Instância única do Serviço REST
         */
        private static RwaResourceAsync instance;
        
        /**
         * @return instância do Serviço RWA
         */
        public static final RwaResourceAsync get() {
            if (instance == null) {
                instance = GWT.create(RwaResourceAsync.class);
                ((RestServiceProxy) instance).setResource(new Resource("/rest/rwa"));
            }
            return instance;
        }
        
        /**
         * 
         */
        private Util() {
        }
    }
    
    /**
     * Adiciona uma classe personalizada pelo
     * usuário
     * 
     * @param customRwa
     * @param callback
     */
    @POST
    void addCustomRwa(String customRwa, MethodCallback<List<String>> callback);
    
    /**
     * Retorna opções de Classes RWA disponíveis
     * 
     * @param callback
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    void getRwaOptions(MethodCallback<List<String>> callback);
    
}
