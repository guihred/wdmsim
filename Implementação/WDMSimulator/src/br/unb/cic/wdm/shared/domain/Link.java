package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidade que representa um enlace entre nós da
 * rede
 * 
 * @author Guilherme
 * 
 */
@Entity(name = "Link")
@Table(name = "LINK")
public class Link implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Largura de banda de um link
     */
    @Column
    private int               bandwidth        = 10000;
    /**
     * Código sequencial gerado para o link
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              codigo;
    /**
     * Representa o atraso de determinado link
     */
    @Column
    private Double            delay            = 1d;
    /**
     * Representa a fonte de um link
     */
    @Column(name = "from_")
    private int               from;
    
    /**
     * Identificador de um link para determinada
     * simulação
     */
    @Column
    private int               id;
    
    /**
     * Destino de detreminado link
     */
    @Column(name = "to_")
    private int               to;
    
    /**
     * Peso de determinado link para a topologia
     */
    @Column
    private int               weight           = 1500;
    
    /**
     * 
     */
    public Link() {
    }
    
    /**
     * @param from
     * @param to
     */
    public Link(int from, int to) {
        this.from = from;
        this.to = to;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Link other = (Link) obj;
        if (codigo == null) {
            if (other.codigo != null)
                return false;
        } else if (!codigo.equals(other.codigo))
            return false;
        return true;
    }
    
    /**
     * @return bandwidth
     */
    public Integer getBandwidth() {
        return bandwidth;
    }
    
    /**
     * @return codigo
     */
    public final Long getCodigo() {
        return codigo;
    }
    
    /**
     * @return delay
     */
    public Double getDelay() {
        return delay;
    }
    
    /**
     * @return from
     */
    public int getFrom() {
        return from;
    }
    
    /**
     * @return id
     */
    public int getId() {
        return id;
    }
    
    /**
     * @return to
     */
    public int getTo() {
        return to;
    }
    
    /**
     * @return weight
     */
    public Integer getWeight() {
        return weight;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
        return result;
    }
    
    /**
     * @param bandwidth
     */
    public void setBandwidth(int bandwidth) {
        this.bandwidth = bandwidth;
    }
    
    /**
     * @param codigo
     */
    public final void setCodigo(Long codigo) {
        this.codigo = codigo;
    }
    
    /**
     * @param delay
     */
    public void setDelay(Double delay) {
        this.delay = delay;
    }
    
    /**
     * @param from
     */
    public void setFrom(int from) {
        this.from = from;
    }
    
    /**
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @param to
     */
    public void setTo(int to) {
        this.to = to;
    }
    
    /**
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    @Override
    public String toString() {
        return "Link [bandwidth=" + bandwidth + ", codigo=" + codigo + ", delay=" + delay + ", from=" + from + ", id="
                + id + ", to=" + to + ", weight=" + weight + "]";
    }
    
}