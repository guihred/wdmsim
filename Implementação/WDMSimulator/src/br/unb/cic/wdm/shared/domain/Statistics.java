package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidade representa as estatísticas geradas
 * pela simulação
 * 
 * @author Guilherme
 * 
 */
@Entity(name = "Statistics")
@Table(name = "STATISTICS")
public class Statistics implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Resultado da execução do printFancy da
     * Classe MyStatistics
     */
    @Column(length = 524288)
    private String            fancy;
    
    /**
     * Identificador das estatísticas
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              id;
    
    /**
     * Resultado da funcão printStatistics na
     * classes MyStatistics
     */
    @Column(length = 524288)
    private String            statistics;
    
    /**
     * 
     */
    public Statistics() {
        fancy = "";
        statistics = "";
    }
    
    /**
     * @param statisticsString
     * @param fancyStatistics
     */
    public Statistics(String statisticsString, String fancyStatistics) {
        this.statistics = statisticsString;
        fancy = fancyStatistics;
    }
    
    /**
     * Lista com o número de chamadas e os pares
     * 
     * @return calls
     */
    public List<String[]> getCalls() {
        List<String[]> list = new ArrayList<String[]>();
        if (fancy != null) {
            
            String[] linha = fancy.split("\n");
            for (String s : linha) {
                if (s.contains("Pair")) {
                    String[] split3 = s.split("\t")[0].split(" ");
                    String[] split4 = split3[1].replace("(", "").replace(")", "").split("->");
                    String split5 = split3[3].replace("(", "").replace(")", "");
                    
                    list.add(new String[] { split4[0], split4[1], split5 });
                }
            }
        } else {
            String[] linha = statistics.split("\n");
            for (String s : linha) {
                if (s.contains("A ")) {
                    String[] split3 = s.split(" ");
                    String[] split4 = split3[0].split("-");
                    
                    list.add(new String[] { split4[0], split4[1], split3[2] });
                }
            }
        }
        
        return list;
    }
    
    /**
     * @return fancy
     */
    public String getFancy() {
        return fancy;
    }
    
    /**
     * @return id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * Lista com o resultado do MyStatistics
     * contendo a Probabilidade média de taxa de
     * bloqueio
     * 
     * 
     * @return mbbr
     */
    public List<String[]> getMBBR() {
        List<String[]> arrayList = new ArrayList<String[]>();
        String[] split = statistics.split("\n");
        for (String s : split) {
            if (s.contains("MBBR")) {
                arrayList.add(s.split(" "));
            }
        }
        
        return arrayList;
    }
    
    /**
     * Lista com a probabilidade média de bloqueio
     * 
     * @return mbp
     */
    public List<String[]> getMBP() {
        List<String[]> arrayList = new ArrayList<String[]>();
        String[] split = statistics.split("\n");
        for (String s : split) {
            if (s.contains("MBP")) {
                arrayList.add(s.split(" "));
            }
        }
        
        return arrayList;
    }
    
    /**
     * @return statistics
     */
    public String getStatistics() {
        return statistics;
    }
    
    /**
     * @param fancyStatistics
     */
    public void setFancy(String fancyStatistics) {
        this.fancy = fancyStatistics;
    }
    
    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @param statisticsString
     */
    public void setStatistics(String statisticsString) {
        this.statistics = statisticsString;
    }
    
    @Override
    public String toString() {
        
        StringBuilder a = new StringBuilder();
        a.append("Statistics [MBBR=");
        List<String[]> calls = getMBBR();
        for (int i = 0; i < calls.size(); i++) {
            String string = calls.get(i)[1];
            a.append(" " + string);
        }
        a.append("]\n");
        return a.toString();
    }
}
