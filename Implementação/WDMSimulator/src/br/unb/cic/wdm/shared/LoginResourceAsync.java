package br.unb.cic.wdm.shared;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.Dispatcher;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import br.unb.cic.wdm.shared.domain.UserData;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;

/**
 * Objeto de chamada a recurso de login
 * 
 * @author Guilherme
 * 
 */
public interface LoginResourceAsync extends RestService {
    
    /**
     * Utility class to get the instance of the
     * Rest Service
     */
    public static final class Util {
        
        /**
         * Referência única ao serviço REST
         */
        private static LoginResourceAsync instance;
        
        /**
         * @return Objeto de chamada a recurso de
         *         login
         */
        public static final LoginResourceAsync get() {
            if (instance == null) {
                instance = GWT.create(LoginResourceAsync.class);
                ((RestServiceProxy) instance).setResource(new Resource("/rest/login"));
                ((RestServiceProxy) instance).setDispatcher(new Dispatcher() {
                    @Override
                    public Request send(Method method, RequestBuilder builder) throws RequestException {
                        System.out.println(method);
                        System.out.println(method.getData());
                        System.out.println(method.getResponse());
                        System.out.println(method.getData());
                        
                        return builder.send();
                    }
                });
                
            }
            return instance;
        }
        
        /**
         * 
         */
        private Util() {
        }
    }
    
    /**
     * Chamada a serviço de requisitar sessão.
     * 
     * @param callback
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    void loginFromSessionServer(MethodCallback<UserData> callback);
    
    /**
     * Chamada a serviço de efetuar login
     * 
     * @param user
     * @param callback
     */
    @POST
    void loginServer(UserData user, MethodCallback<UserData> callback);
    
    /**
     * Chamada a serviço de sair do login
     * 
     * @param callback
     */
    @DELETE
    void logout(MethodCallback<Void> callback);
}
