package br.unb.cic.wdm.shared;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import br.unb.cic.wdm.shared.domain.Simulation;

import com.google.gwt.core.client.GWT;

/**
 * Interface que permite fazer chamdas remotas a
 * serviços e recursos de arquivos
 * 
 * @author Guilherme
 * 
 */
public interface FileResourceAsync extends RestService {
    
    /**
     * Utility class to get the instance of the
     * Rest Service
     */
    public static final class Util {
        
        /**
         * Referência única da classe
         */
        private static FileResourceAsync instance;
        
        /**
         * @return instance do serviço
         */
        public static final FileResourceAsync get() {
            if (instance == null) {
                instance = GWT.create(FileResourceAsync.class);
                ((RestServiceProxy) instance).setResource(new Resource("/rest/file"));
                
            }
            return instance;
        }
        
        /**
         * 
         */
        private Util() {
        }
    }
    
    /**
     * Permite abrir uma simulação já salva em
     * banco
     * 
     * @param idSimulation
     * @param callback
     */
    @GET
    @Path("/{id}")
    void openSimulation(@PathParam("id") Long idSimulation, MethodCallback<Simulation> callback);
    
    /**
     * Permite salvar uma simulação em banco
     * 
     * @param simulation
     * @param callback
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    void saveSimulation(Simulation simulation, MethodCallback<Long> callback);
}
