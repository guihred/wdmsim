package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entidade que contém dados da simulação a ser
 * executada e armazenada
 * 
 * @author Guilherme
 * 
 */
@Entity(name = "Simulation")
@Table(name = "SIMULATION")
public class Simulation implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Chamadas de uma simulação
     */
    @JoinColumn
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    private List<Calls>       calls            = new LinkedList<Calls>();
    
    /**
     * Identificador de uma simulação
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              id;
    
    /**
     * Links da rede modelada
     */
    @JoinColumn
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    private List<Link>        links            = new LinkedList<Link>();
    
    /**
     * Taxa máxima da rede simulada
     */
    @Column
    private Integer           maxRate          = 10000;
    
    /**
     * Nós contidos na simulação
     */
    @JoinColumn
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    private List<Node>        nodes            = new LinkedList<Node>();
    
    /**
     * Nome físico da topologia
     */
    @Column
    private String            physicalName     = "NSFNet";
    
    /**
     * Nome do módulo rwa
     */
    @Column
    private String            rwaModule        = "My2RWA";
    
    /**
     * Estatísticas geradas para a simulação
     */
    @JoinColumn
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    private List<Statistics>  statistics;
    
    /**
     * Arquivo de trace
     */
    @Column
    private String            traceFile        = "trace.tr";
    /**
     * número de chamadas para determinado tráfego
     */
    @Column
    private Integer           trafficCalls     = 10000;
    /**
     * Número execuções da simulação
     */
    @Column
    private Integer           trafficLoad      = 10;
    /**
     * Nome da topologia virtual
     */
    @Column
    private String            virtualName      = "NSFNet";
    /**
     * número de comprimentos de onda
     */
    @Column
    private Integer           wavelengths      = 16;
    
    /**
     * Número de versão do simulador
     */
    @Column
    private String            wdmVersion       = "0.1";
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Simulation other = (Simulation) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
    
    /**
     * @return calls
     */
    public List<Calls> getCalls() {
        return calls;
    }
    
    /**
     * @return id
     */
    public Long getId() {
        return id;
    }
    
    /**
     * @return links
     */
    public List<Link> getLinks() {
        return links;
    }
    
    /**
     * @return maior taxa dentro da lista de
     *         chamadas
     */
    @Transient
    public Integer getMaxRate() {
        maxRate = 0;
        for (Calls call : calls) {
            maxRate = (maxRate > call.getRate()) ? maxRate : call.getRate();
        }
        return maxRate;
    }
    
    /**
     * @return nodes
     */
    public List<Node> getNodes() {
        return nodes;
    }
    
    /**
     * @return physicalName
     */
    public String getPhysicalName() {
        return physicalName;
    }
    
    /**
     * @return rwaModule
     */
    public String getRwaModule() {
        return rwaModule;
    }
    
    /**
     * @return statistics
     */
    public List<Statistics> getStatistics() {
        return statistics;
    }
    
    /**
     * @return traceFile
     */
    public String getTraceFile() {
        return traceFile;
    }
    
    /**
     * @return trafficCalls
     */
    public Integer getTrafficCalls() {
        return trafficCalls;
    }
    
    /**
     * @return trafficLoad
     */
    public Integer getTrafficLoad() {
        return trafficLoad;
    }
    
    /**
     * @return virtualName
     */
    public String getVirtualName() {
        return virtualName;
    }
    
    /**
     * @return wavelengths
     */
    public Integer getWavelengths() {
        return wavelengths;
    }
    
    /**
     * @return wdmVersion
     */
    public String getWdmVersion() {
        return wdmVersion;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    /**
     * @param calls
     */
    public void setCalls(List<Calls> calls) {
        this.calls = calls;
    }
    
    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @param links
     */
    public void setLinks(List<Link> links) {
        this.links = links;
    }
    
    /**
     * @param maxRate
     */
    public void setMaxRate(Integer maxRate) {
        this.maxRate = maxRate;
    }
    
    /**
     * @param nodes
     */
    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }
    
    /**
     * @param physicalName
     */
    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }
    
    /**
     * @param rwaModule
     */
    public void setRwaModule(String rwaModule) {
        this.rwaModule = rwaModule;
    }
    
    /**
     * @param statistics
     */
    public void setStatistics(List<Statistics> statistics) {
        this.statistics = statistics;
    }
    
    /**
     * @param traceFile
     */
    public void setTraceFile(String traceFile) {
        this.traceFile = traceFile;
    }
    
    /**
     * @param trafficCalls
     */
    public void setTrafficCalls(Integer trafficCalls) {
        this.trafficCalls = trafficCalls;
    }
    
    /**
     * @param trafficLoad
     */
    public void setTrafficLoad(Integer trafficLoad) {
        this.trafficLoad = trafficLoad;
    }
    
    /**
     * @param virtualName
     */
    public void setVirtualName(String virtualName) {
        this.virtualName = virtualName;
    }
    
    /**
     * @param wavelengths
     */
    public void setWavelengths(Integer wavelengths) {
        this.wavelengths = wavelengths;
    }
    
    /**
     * @param wdmVersion
     */
    public void setWdmVersion(String wdmVersion) {
        this.wdmVersion = wdmVersion;
    }
    
    @Override
    public String toString() {
        return "Simulation [calls=" + calls + ", links=" + links + ", maxRate=" + maxRate + ", nodes=" + nodes
                + ", physicalName=" + physicalName + ", rwaModule=" + rwaModule + ", traceFile=" + traceFile
                + ", trafficCalls=" + trafficCalls + ", trafficLoad=" + trafficLoad + ", virtualName=" + virtualName
                + ", wavelengths=" + wavelengths + ", wdmVersion=" + wdmVersion + "]";
    }
}
