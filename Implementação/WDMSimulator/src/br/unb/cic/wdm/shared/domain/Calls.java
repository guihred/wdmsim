package br.unb.cic.wdm.shared.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entidade que representa uma chamada a
 * determinado serviço da rede
 * 
 * @author Guilherme
 * 
 */
@Entity(name = "Calls")
@Table(name = "CALLS")
public class Calls implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Classe de serviço
     */
    @Column
    private Integer           cos              = 0;
    /**
     * Tempo Médio de duração
     */
    @Column
    private Integer           holdingTime      = 1;
    
    /**
     * id da chamada
     */
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              id;
    
    /**
     * Taxa de execução da chamada
     */
    @Column
    private Integer           rate             = 1000;
    
    /**
     * Peso da chamada
     */
    @Column
    private Integer           weight           = 1;
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Calls other = (Calls) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
    
    /**
     * @return cos
     */
    public final Integer getCos() {
        return cos;
    }
    
    /**
     * @return holdingTime
     */
    public final Integer getHoldingTime() {
        return holdingTime;
    }
    
    /**
     * @return id
     */
    public final Long getId() {
        return id;
    }
    
    /**
     * @return rate
     */
    public final Integer getRate() {
        return rate;
    }
    
    /**
     * @return weight
     */
    public final Integer getWeight() {
        return weight;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    /**
     * @param cos
     */
    public final void setCos(Integer cos) {
        this.cos = cos;
    }
    
    /**
     * @param holdingTime
     */
    public final void setHoldingTime(Integer holdingTime) {
        this.holdingTime = holdingTime;
    }
    
    /**
     * @param id
     */
    public final void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @param rate
     */
    public final void setRate(Integer rate) {
        this.rate = rate;
    }
    
    /**
     * @param i
     */
    public final void setWeight(Integer i) {
        this.weight = i;
    }
    
    @Override
    public String toString() {
        return "Calls [cos=" + cos + ", holdingTime=" + holdingTime + ", id=" + id + ", rate=" + rate + ", weight="
                + weight + "]";
    }
    
}
