package br.unb.cic.wdm.server;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.unb.cic.wdm.shared.domain.Simulation;
import br.unb.cic.wdm.shared.domain.Statistics;

import com.sun.jersey.spi.resource.Singleton;

/**
 * 
 * Recurso de Estatisticas da simulação
 * 
 * 
 * @author Guilherme
 * 
 */
@Path("/stats")
@Singleton
public class StatisticsResource {
    
    /**
     * Recuperar determinada estatística gerada
     * pela simulação
     * 
     * @param idSimulation
     * @return lista de estatísticas executadas
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Statistics> openStatistics(@PathParam("id") Long idSimulation) {
        Simulation simulation = SimulationUtils.getSimulations().get(idSimulation);
        if(simulation==null){
        	return null;
        }
        List<Statistics> statistics = simulation.getStatistics();
        return statistics;
    }
    
}