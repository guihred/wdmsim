package br.unb.cic.wdm.server;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import br.unb.cic.wdm.shared.domain.Simulation;

/**
 * Servlet responsável por receber submissão de
 * arquivo de simulação
 * 
 * @author Guilherme
 */
public class UploadSimulationServlet extends HttpServlet {
    
    /**
     * 
     */
    private static final Logger log              = Logger.getLogger(UploadSimulationServlet.class.getName());
    
    /**
     * Tamanho máximo de arquivo
     */
    private static final int    MAX_FILE_SIZE    = 500 * 1024;
    
    // final BlobstoreService blobstoreService =
    // BlobstoreServiceFactory.getBlobstoreService();
    
    /**
     * Tamanho máximo de arquivo em memória
     */
    private static final int    MAX_MEN_SIZE     = 400 * 1024;
    /**
     * 
     */
    private static final long   serialVersionUID = 1L;
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Long parameter = null;
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(MAX_MEN_SIZE);
            File tempFile = File.createTempFile("temp", ".xml");
            log.fine(tempFile.getAbsolutePath());
            factory.setRepository(tempFile);
            ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
            
            servletFileUpload.setSizeMax(MAX_FILE_SIZE);
            
            List<?> parseRequest = servletFileUpload.parseRequest(request);
            
            if (parseRequest != null && parseRequest.size() > 0) {
                for (Object o : parseRequest) {
                    FileItem object = (FileItem) o;
                    String fieldName = object.getFieldName();
                    if (fieldName.equals("client")) {
                        parameter = Long.valueOf(object.getString());
                    }
                    if (fieldName.equals("upload")) {
                        byte[] fetchData = new byte[Long.valueOf(object.getSize()).intValue()];
                        object.getInputStream().read(fetchData);
                        String content = new String(fetchData);
                        
                        Simulation simulation = SimulationUtils.parseSimulation(content);
                        SimulationUtils.getSimulations().put(parameter, simulation);
                        
                    }
                }
            }
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            response.setContentType("text/html");
            response.flushBuffer();
            response.getWriter().close();
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    
}
