package br.unb.cic.wdm.server;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import br.unb.cic.wdm.shared.domain.UserData;

/**
 * Recurso de efetuação e controle de login
 * 
 * @author Guilherme
 * 
 */
@Path("/login")
public class LoginResource {
    
    /**
     * Retorna informações de usuário autenticado
     * e com chave de sessão ativa
     * 
     * @param request
     * @return dados de usuário
     */
    @GET
    public UserData loginFromSessionServer(@Context HttpServletRequest request) {
        UserData user = null;
        System.out.println("login resource");
        HttpSession session = request.getSession();
        Object userObj = session.getAttribute("user");
        if (userObj != null && userObj instanceof UserData) {
            user = (UserData) userObj;
        }
        Principal userPrincipal = request.getUserPrincipal();
        System.out.println((userPrincipal != null) ? userPrincipal.getName() : null);
        
        return user;
    }
    
    /**
     * Verifica tentativa de login de usuário
     * 
     * @param user
     * @param request
     * @return dados do usuário
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserData loginServer(UserData user, @Context HttpServletRequest request) {
        
        // validate username and password
        String login = user.getLogin();
        // TODO
        user.setLoggedIn(true);
        user.setSessionId("1");
        // store the user/session id
        HttpSession session = request.getSession(true);
        session.setAttribute("user", user);
        
        return user;
    }
    
    /**
     * Deleta registro de sessão aberta
     * 
     * @param request
     */
    @DELETE
    public void logout(@Context HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("user");
    }
    
}
