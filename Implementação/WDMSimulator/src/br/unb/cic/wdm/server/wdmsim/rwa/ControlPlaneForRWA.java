/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.unb.cic.wdm.server.wdmsim.rwa;

import java.util.Map;

import br.unb.cic.wdm.server.wdmsim.Flow;
import br.unb.cic.wdm.server.wdmsim.LightPath;
import br.unb.cic.wdm.server.wdmsim.Path;

/**
 * This is the interface that provides several methods for the RWA Class within
 * the Control Plane.
 * 
 * @author andred
 */
public interface ControlPlaneForRWA {
    
    public boolean acceptFlow(long id, LightPath[] lightpaths);
    
    public boolean blockFlow(long id);
    
    public Flow getFlow(long id);
    
    public int getLightpathFlowCount(long id);
    
    public Map<Flow, Path> getMappedFlows();
    
    public Path getPath(Flow flow);
    
    public boolean rerouteFlow(long id, LightPath[] lightpaths);
}
