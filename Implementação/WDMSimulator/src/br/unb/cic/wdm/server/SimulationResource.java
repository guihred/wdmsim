package br.unb.cic.wdm.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.unb.cic.wdm.server.wdmsim.MyStatistics;
import br.unb.cic.wdm.server.wdmsim.Simulator;
import br.unb.cic.wdm.server.wdmsim.SimulatorGWT;
import br.unb.cic.wdm.server.wdmsim.Tracer;
import br.unb.cic.wdm.shared.domain.Simulation;
import br.unb.cic.wdm.shared.domain.Statistics;

/**
 * Classe que disponibiliza serviços para a
 * execução de simulação
 * 
 * @author Guilherme
 * 
 */
@Path("/sim")
public class SimulationResource {
    
    /**
     * Thread que permite execução várias cargas
     * de simulação paralelamente
     * 
     * @author Guilherme
     * 
     */
	private final int N_THREADS = 10;
    private class SimulationThread extends Thread {
        
        /**
         * id da simulação a ser executada
         */
        private final Long idSimulation;
        /**
         * estatística gerada pela simulação
         */
        private Statistics statistic;
		private int load;
        
        /**
         * @param idSimulation
         */
        public SimulationThread(Long idSimulation, int load) {
            this.idSimulation = idSimulation;
            this.load = load;
        }
        
        /**
         * @return statistic
         */
        public Statistics getStatistics() {
            return statistic;
        }
        
        @Override
        public void run() {
            Random random = new Random();
            MyStatistics myStatistics = new MyStatistics();
            Tracer tracer = new Tracer();
            try {
                int seed = random.nextInt(25) + 1;
                for (int i = 0; i < load; i++) {

                	SimulatorGWT wdm = new SimulatorGWT();
                	wdm.setStats(myStatistics);
                	wdm.setTracer(tracer);
                	wdm.Execute(idSimulation, false, false, 0, seed);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.statistic = myStatistics.asStatistics();
        }
        
    };
    
    /**
     * @return novo id de simulação disponível
     *         para salvamento
     */
    @GET
    @Path("id")
    @Produces(MediaType.APPLICATION_JSON)
    public String createUploadUrl() {
        String url = SimulationUtils.getSimulations().newId().toString();
        return url;
    }
    
    /**
     * Executa a simulação e realiza o seu
     * salvamento em banco
     * 
     * @param simulation
     * @return id de simulação executada
     * @throws Exception
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Long executeSimulation(Simulation simulation) throws Exception {
        Long idSimulation = SimulationUtils.getSimulations().newId();
        byte[] parseSimulation = SimulationUtils.parseSimulation(simulation);
        SimulationUtils.getSimConfigFile().put(idSimulation, parseSimulation);
//        List<SimulationThread> threads = new ArrayList<SimulationThread>();
//        for (int i = 0; i < N_THREADS; i++) {
//            SimulationThread thread = new SimulationThread(idSimulation,simulation.getTrafficLoad());
//            threads.add(thread);
//            thread.start();
//        }
        List<Statistics> statisticsList = new ArrayList<Statistics>();
		for (int i = 0; i < N_THREADS; i++) {
			Random random = new Random();
			MyStatistics myStatistics = new MyStatistics();
	        Tracer tracer = new Tracer();
	        SimulatorGWT wdm = new SimulatorGWT();
	        wdm.setStats(myStatistics);
	        wdm.setTracer(tracer);
	        for (double j = 0; j < simulation.getTrafficLoad(); j++) {
	        	int seed = random.nextInt(25) + 1;
	        	wdm.Execute(idSimulation, false, false, j, seed);
	        }
	        statisticsList.add(myStatistics.asStatistics());
		}

		simulation.setStatistics(statisticsList);
        SimulationUtils.getSimulations().put(idSimulation, simulation);
        return idSimulation;
    }
}