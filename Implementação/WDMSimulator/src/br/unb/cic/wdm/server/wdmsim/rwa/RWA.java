/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.unb.cic.wdm.server.wdmsim.rwa;

import br.unb.cic.wdm.server.wdmsim.Flow;
import br.unb.cic.wdm.server.wdmsim.PhysicalTopology;
import br.unb.cic.wdm.server.wdmsim.VirtualTopology;

/**
 * This is the interface that provides some methods for the RWA class. These
 * methods basically deal with the simulation interface and with the arriving
 * and departing flows.
 * 
 * The Routing and Wavelength Assignment (RWA) is a optical networking problem
 * that has the goal of maximizing the number of optical connections.
 * 
 * @author andred
 */
public interface RWA {
    
    public void flowArrival(Flow flow);
    
    public void flowDeparture(long id);
    
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp);
    
}
