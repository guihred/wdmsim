/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unb.cic.wdm.server.wdmsim;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import br.unb.cic.wdm.server.SimulationUtils;

/**
 * Centralizes the simulation execution. Defines
 * what the command line arguments do, and
 * extracts the simulation information from the
 * XML file.
 * 
 * @author andred
 */
public class SimulatorGWT {
    
    private static final String simName    = new String("wdmsim");
    private static final Float  simVersion = new Float(0.1);
    private EventScheduler      events;
    private MyStatistics        stats;
    public boolean              trace      = false;
    public boolean              verbose    = false;
    private Tracer tr ;
    public void Execute(Long simConfigFile, boolean trace, boolean verbose, double forcedLoad, int seed)
            throws Exception {
        
        this.verbose = verbose;
        this.trace = trace;
        
        if (this.verbose) {
            System.out.println("#################################");
            System.out.println("# Simulator " + simName + " version " + simVersion.toString() + "  #");
            System.out.println("#################################\n");
        }
        
        long begin = System.currentTimeMillis();
        
        if (this.verbose) {
            System.out.println("(0) Accessing simulation file " + simConfigFile + "...");
        }
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(new InputSource(new StringReader(new String(SimulationUtils.getSimConfigFile()
                .get(simConfigFile)))));
        
        // normalize text representation
        doc.getDocumentElement().normalize();
        
        // check th e root TAG name and version
        if (!doc.getDocumentElement().getNodeName().equals(simName)) {
            System.out.println("Root element of the simulation file is " + doc.getDocumentElement().getNodeName()
                    + ", " + simName + " is expected!");
            System.exit(0);
        }
        if (!doc.getDocumentElement().hasAttribute("version")) {
            System.out.println("Cannot find version attribute!");
            System.exit(0);
        }
        if (Float.compare(new Float(doc.getDocumentElement().getAttribute("version")), simVersion) > 0) {
            System.out.println("Simulation config file requires a newer version of the simulator!");
            System.exit(0);
        }
        if (this.verbose) {
            System.out.println("(0) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        
        /* Extract physical topology part */
        begin = System.currentTimeMillis();
        if (this.verbose) {
            System.out.println("(1) Loading physical topology information...");
        }
        
        PhysicalTopology pt = new PhysicalTopology((Element) doc.getElementsByTagName("physical-topology").item(0));
        if (this.verbose) {
            System.out.println(pt);
        }
        
        if (this.verbose) {
            System.out.println("(1) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        
        /* Extract virtual topology part */
        begin = System.currentTimeMillis();
        if (this.verbose) {
            System.out.println("(2) Loading virtual topology information...");
        }
        if(tr==null){
        	tr = new Tracer();
        }
        
        VirtualTopology vt = new VirtualTopology((Element) doc.getElementsByTagName("virtual-topology").item(0), pt, tr);
        if (this.verbose) {
            System.out.println(vt);
        }
        
        if (this.verbose) {
            System.out.println("(2) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        
        /* Extract simulation traffic part */
        begin = System.currentTimeMillis();
        if (this.verbose) {
            System.out.println("(3) Loading traffic information...");
        }
        
        events = new EventScheduler();
        TrafficGenerator traffic = new TrafficGenerator((Element) doc.getElementsByTagName("traffic").item(0),
                forcedLoad);
        traffic.generateTraffic(pt, events, seed);
        
        if (this.verbose) {
            System.out.println("(3) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        
        /* Extract simulation setup part */
        begin = System.currentTimeMillis();
        if (this.verbose) {
            System.out.println("(4) Loading simulation setup information...");
        }
        if (stats == null) {
            stats = new MyStatistics();
        }
        
        
        stats.statisticsSetup(pt.getNumNodes(), 3, 0);
        if (this.trace == true) {
        	if (forcedLoad == 0) {
                tr.setTraceFile(simConfigFile + ".trace");
            } else {
                tr.setTraceFile(simConfigFile + "_Load_" + Double.toString(forcedLoad) + ".trace");
            }
        }
        tr.toogleTraceWriting(this.trace);
        
        String rwaModule = "br.unb.cic.wdm.server.wdmsim.rwa."
                + ((Element) doc.getElementsByTagName("rwa").item(0)).getAttribute("module");
        if (this.verbose) {
            System.out.println("RWA module: " + rwaModule);
        }
        ControlPlane cp = new ControlPlane(rwaModule, pt, vt, stats, tr);
        
        if (this.verbose) {
            System.out.println("(4) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        /* Run the simulation */
        begin = System.currentTimeMillis();
        if (this.verbose) {
            System.out.println("(5) Running the simulation...");
        }
        
        new SimulationRunner(cp, events, stats, tr);
        
        if (this.verbose) {
            System.out.println("(5) Done. ("
                    + Float.toString((float) (System.currentTimeMillis() - begin) / (float) 1000) + " sec)\n");
        }
        
        if (this.verbose) {
            if (forcedLoad == 0) {
                System.out.println("Statistics (" + simConfigFile + "):\n");
            } else {
                System.out.println("Statistics for " + Double.toString(forcedLoad) + " erlangs (" + simConfigFile
                        + "):\n");
            }
            System.out.println(stats.fancyStatistics());
        } else {
            // System.out.println("*****");
            // if (forcedLoad != 0) {
            // System.out.println("Load:" +
            // Double.toString(forcedLoad));
            // }
            // stats.printStatistics();
        }
        
        // Terminate MyStatistics singleton
        stats.finish();
        // System.out.println(stats.fancyStatistics());
        // Flush and close the trace file and
        // terminate the singleton
        if (this.trace == true)
            tr.finish();
        
    }
    
    public EventScheduler getEvents() {
        return events;
    }
    
    public MyStatistics getStats() {
        return stats;
    }
    
    /**
     * Executes simulation based on the given XML
     * file and the used command line arguments.
     * 
     * @param simConfigFile
     *            name of the XML file that
     *            contains all information about
     *            the simulation
     * @param trace
     *            activates the Tracer class
     *            functionalities
     * @param verbose
     *            activates the printing of
     *            information about the
     *            simulation, on runtime, for
     *            debugging purposes
     * @param forcedLoad
     *            range of loads for which several
     *            simulations are automated; if
     *            not specified, load is taken
     *            from the XML file
     * @param seed
     *            a number in the interval [1,25]
     *            that defines up to 25 different
     *            random simulations
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public void setStats(MyStatistics stats) {
        this.stats = stats;
    }
    public void setTracerFile(PrintWriter writer ) throws IOException{
    	tr.setTraceFile(writer);
    }
    public void setTracer(Tracer tracer ) throws IOException{
    	tr=tracer;
    }
    
    
    
}
