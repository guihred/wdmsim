/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.unb.cic.wdm.server.wdmsim.rwa;

import br.unb.cic.wdm.server.wdmsim.Flow;
import br.unb.cic.wdm.server.wdmsim.LightPath;
import br.unb.cic.wdm.server.wdmsim.PhysicalTopology;
import br.unb.cic.wdm.server.wdmsim.VirtualTopology;
import br.unb.cic.wdm.server.wdmsim.util.Dijkstra;
import br.unb.cic.wdm.server.wdmsim.util.WeightedGraph;

/**
 * This is a sample algorithm for the Routing and Wavelength Assignment problem.
 * 
 * Fixed path routing is the simplest approach to finding a lightpath. The same
 * fixed route for a given source and destination pair is always used. This path
 * is computed using Dijkstra's Algorithm.
 * 
 * First-Fit wavelength assignment tries to establish the lightpath using the
 * first wavelength available sought in the increasing order.
 */
public class MyRWA implements RWA {
    
    private ControlPlaneForRWA cp;
    private WeightedGraph      graph;
    private PhysicalTopology   pt;
    private VirtualTopology    vt;
    
    public void flowArrival(Flow flow) {
        int[] nodes;
        int[] links;
        int[] wvls;
        long id;
        LightPath[] lps = new LightPath[1];
        ;
        
        // Shortest-Path routing
        nodes = Dijkstra.getShortestPath(graph, flow.getSource(), flow.getDestination());
        
        // If no possible path found, block the call
        if (nodes.length == 0) {
            cp.blockFlow(flow.getID());
            return;
        }
        
        // Create the links vector
        links = new int[nodes.length - 1];
        for (int j = 0; j < nodes.length - 1; j++) {
            links[j] = pt.getLink(nodes[j], nodes[j + 1]).getID();
        }
        
        // First-Fit wavelength assignment
        wvls = new int[links.length];
        for (int i = 0; i < pt.getNumWavelengths(); i++) {
            // Create the wavelengths vector
            for (int j = 0; j < links.length; j++) {
                wvls[j] = i;
            }
            // If can establish the lightpath, accept the call
            if ((id = vt.createLightpath(links, wvls)) >= 0) {
                // Single-hop routing (end-to-end lightpath)
                lps[0] = vt.getLightpath(id);
                cp.acceptFlow(flow.getID(), lps);
                return;
            }
        }
        // Block the call
        cp.blockFlow(flow.getID());
    }
    
    @Override
    public void flowDeparture(long id) {
    }
    
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp) {
        this.pt = pt;
        this.vt = vt;
        this.cp = cp;
        this.graph = pt.getWeightedGraph();
    }
    
}
