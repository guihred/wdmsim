package br.unb.cic.wdm.server;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unb.cic.wdm.shared.domain.Simulation;

/**
 * Servlet permite ao usuário recuperar um arquivo
 * XML com parâmetros de simulação.
 * 
 * @author Guilherme
 * 
 */
public class DownloadServlet extends HttpServlet {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String chave = req.getParameter("chave");
            Long id = Long.valueOf(chave);
            resp.setContentType("application/xml");
            
            if (req.getHeader("User-Agent").contains("MSIE")) {
                resp.setHeader("Content-Disposition", "inline;filename=" + "\"" + "default.xml" + "\"");
                resp.setHeader("Cache-Control", "no-cache");
            } else {
                resp.setHeader("Content-Disposition", "attachment;filename=" + "\"" + "default.xml" + "\"");
            }
            
            resp.setCharacterEncoding("UTF-8");
            Writer outputStream = resp.getWriter();
            Simulation simulation = SimulationUtils.getSimulations().get(id);
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            resp.setHeader("Pragma", "public");            // resp.set
            byte[] parseSimulation = SimulationUtils.parseSimulation(simulation);
            resp.setContentLength(parseSimulation.length);
            String string = new String(parseSimulation);
            outputStream.write(string);
            outputStream.flush();
            outputStream.close();
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    
}