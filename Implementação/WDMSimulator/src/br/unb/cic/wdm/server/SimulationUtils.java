package br.unb.cic.wdm.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.xml.bind.ValidationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import br.unb.cic.wdm.server.wdmsim.rwa.MyRWA;
import br.unb.cic.wdm.server.wdmsim.rwa.RWA;
import br.unb.cic.wdm.shared.domain.Calls;
import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;
import br.unb.cic.wdm.shared.domain.Simulation;

/**
 * Classe com funções de conversão e funções
 * relevantes à execução de simulação
 * 
 * @author Guilherme
 * 
 */
public final class SimulationUtils {
    /**
     * Lista de classes RWA personalizadas
     */
    private final static List<Class<? extends RWA>> customRwas           = new ArrayList<Class<? extends RWA>>();
    /**
     * Mapa com arquivos de configuração de
     * simulação
     */
    private final static Hashtable<Long, byte[]>    simulationConfigFile = new Hashtable<>(1000);
    // private final static Hashtable<Long,
    // EventScheduler> simulationEvents = new
    // Hashtable<>(1000);
    /**
     * Referência a DAO que realiza acesso ao
     * banco de dados
     */
    private final static SimulationDaoMap           simulations          = new SimulationDaoMap();
    
    /**
     * @param packageName
     * @return Lista de classes dentro de
     *         determinado pacote
     * @throws Exception
     */
    public static final List<Class<?>> getClassesFromPackage(String packageName) throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class<?>> classes = new ArrayList<Class<?>>();
        for (File directory : dirs) {
            List<Class<?>> findClasses = new ArrayList<Class<?>>();
            File[] files = directory.listFiles();
            for (File file : files) {
                if (!file.isDirectory() && file.getName().endsWith(".class")) {
                    findClasses.add(Class.forName(packageName + '.'
                            + file.getName().substring(0, file.getName().length() - 6)));
                }
            }
            classes.addAll(findClasses);
        }
        return classes;
    }
    
    /**
     * @return lista de classes RWA personalizadas
     */
    public final static List<Class<? extends RWA>> getCustomRwas() {
        return customRwas;
    }
    
    /**
     * Busca determinada classe RWA dentro da
     * lista
     * 
     * @param rwaClassName
     * @return encontrada
     */
    public final static Class<? extends RWA> getRwas(String rwaClassName) {
        for (Class<? extends RWA> rwa : customRwas) {
            if (rwa.getSimpleName().equals(rwaClassName)) {
                return rwa;
            }
        }
        
        return MyRWA.class;
    }
    
    /**
     * @return mapa com arquivos de configuração
     *         de simulação
     */
    public synchronized final static Hashtable<Long, byte[]> getSimConfigFile() {
        return simulationConfigFile;
    }
    
    // public synchronized final static
    // Hashtable<Long, EventScheduler>
    // getSimEvents() {
    // return simulationEvents;
    // }
    //
    /**
     * @return DAO que acessa o banco
     */
    public synchronized final static SimulationDaoMap getSimulations() {
        return simulations;
    }
    
    /**
     * Normaliza a simulação retirando
     * redundâncias e parâmetros não utilizados
     * 
     * @param simulation
     */
    private static void normalizeSimulation(Simulation simulation) {
        for (int i = 0; i < simulation.getNodes().size(); i++) {
            if (!simulation.getNodes().get(i).isActive()) {
                simulation.getNodes().remove(i);
                i--;
            }
        }
        for (int i = 0; i < simulation.getNodes().size(); i++) {
            Node node = simulation.getNodes().get(i);
            if (node.getId() != i) {
                List<Link> links = simulation.getLinks();
                for (Link link : links) {
                    if (link.getTo() == node.getId()) {
                        link.setTo(i);
                    }
                    if (link.getFrom() == node.getId()) {
                        link.setFrom(i);
                    }
                }
                node.setId(i);
            }
        }
    }
    
    /**
     * @param simulation
     * @return cria arquivo XML com os parâmetros
     *         de simulação
     */
    public static byte[] parseSimulation(Simulation simulation) {
        try {
            normalizeSimulation(simulation);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            doc.setXmlStandalone(true);
            Element wdmsim = doc.createElement("wdmsim");
            wdmsim.setAttribute("version", simulation.getWdmVersion());
            Element rwa = doc.createElement("rwa");
            rwa.setAttribute("module", simulation.getRwaModule());
            Element trace = doc.createElement("trace");
            trace.setAttribute("file", simulation.getTraceFile());
            Element traffic = doc.createElement("traffic");
            traffic.setAttribute("calls", simulation.getTrafficCalls().toString());
            traffic.setAttribute("load", simulation.getTrafficLoad().toString());
            traffic.setAttribute("max-rate", simulation.getMaxRate().toString());
            for (Calls call : simulation.getCalls()) {
                Element eNode = doc.createElement("calls");
                eNode.setAttribute("holding-time", call.getHoldingTime().toString());
                eNode.setAttribute("rate", call.getRate().toString());
                eNode.setAttribute("cos", call.getCos().toString());
                eNode.setAttribute("weight", call.getWeight().toString());
                traffic.appendChild(eNode);
            }
            Element virtual = doc.createElement("virtual-topology");
            virtual.setAttribute("name", simulation.getVirtualName());
            Element physical = doc.createElement("physical-topology");
            physical.setAttribute("name", simulation.getPhysicalName());
            physical.setAttribute("wavelengths", simulation.getWavelengths().toString());
            Element nodes = doc.createElement("nodes");
            physical.appendChild(nodes);
            for (br.unb.cic.wdm.shared.domain.Node node : simulation.getNodes()) {
                if (node.isActive()) {
                    Element eNode = doc.createElement("node");
                    eNode.setAttribute("id", ((Integer) node.getId()).toString());
                    eNode.setAttribute("grooming-in-ports", node.getGroomingIn().toString());
                    eNode.setAttribute("grooming-out-ports", node.getGroomingOut().toString());
                    eNode.setAttribute("wlconverters", node.getWlConverter().toString());
                    eNode.setAttribute("wlconversion-range", node.getConversionRange().toString());
                    nodes.appendChild(eNode);
                }
            }
            Element links = doc.createElement("links");
            physical.appendChild(links);
            for (int i = 0; i < simulation.getLinks().size(); i++) {
                Link link = simulation.getLinks().get(i);
                Element eLink = doc.createElement("link");
                eLink.setAttribute("id", Integer.toString(i));
                eLink.setAttribute("source", ((Integer) link.getFrom()).toString());
                eLink.setAttribute("destination", ((Integer) link.getTo()).toString());
                eLink.setAttribute("delay", link.getDelay().toString());
                eLink.setAttribute("bandwidth", link.getBandwidth().toString());
                eLink.setAttribute("weight", link.getWeight().toString());
                links.appendChild(eLink);
            }
            
            wdmsim.appendChild(rwa);
            wdmsim.appendChild(trace);
            wdmsim.appendChild(traffic);
            wdmsim.appendChild(virtual);
            wdmsim.appendChild(physical);
            
            doc.appendChild(wdmsim);
            doc.normalize();
            
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(bos));
            byte[] array = bos.toByteArray();
            
            return new String(array).replace(">", ">\n").getBytes();
        } catch (Exception e) {
        }
        return null;
    }
    
    /**
     * Cria objeto de simulação a partir de
     * arquivo
     * 
     * @param content
     * @return simulation
     * @throws Exception
     */
    public static Simulation parseSimulation(String content) throws Exception {
        Simulation simulation = new Simulation();
        try {
            
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.parse(new InputSource(new StringReader(content)));
            NodeList nList = doc.getElementsByTagName("wdmsim");
            
            if (nList.getLength() > 0 && nList.item(0).getNodeName().equals("wdmsim")) {
                simulation.setWdmVersion(((Element) nList.item(0)).getAttribute("version"));
                nList = nList.item(0).getChildNodes();
            }
            for (int temp = 0; temp < nList.getLength(); temp++) {
                
                org.w3c.dom.Node nNode = nList.item(temp);
                
                if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) nNode;
                    String nodeName = eElement.getNodeName();
                    if (nodeName.equals("rwa")) {
                        simulation.setRwaModule(eElement.getAttribute("module"));
                    }
                    if (nodeName.equals("trace")) {
                        simulation.setTraceFile(eElement.getAttribute("file"));
                    }
                    if (nodeName.equals("traffic")) {
                        try {
                            simulation.setTrafficCalls(Integer.parseInt(eElement.getAttribute("calls")));
                            simulation.setTrafficLoad(Integer.parseInt(eElement.getAttribute("load")));
                            simulation.setMaxRate(Integer.parseInt(eElement.getAttribute("max-rate")));
                            NodeList trafficChildren = eElement.getChildNodes();
                            for (int j = 0; j < trafficChildren.getLength(); j++) {
                                if (trafficChildren.item(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE
                                        && trafficChildren.item(j).getNodeName().equals("calls")) {
                                    Element element = (Element) trafficChildren.item(j);
                                    Calls calls = new Calls();
                                    calls.setHoldingTime(Integer.parseInt(element.getAttribute("holding-time")));
                                    calls.setRate(Integer.parseInt(element.getAttribute("rate")));
                                    calls.setCos(Integer.parseInt(element.getAttribute("cos")));
                                    calls.setWeight(Integer.parseInt(element.getAttribute("weight")));
                                    simulation.getCalls().add(calls);
                                }
                            }
                        } catch (NumberFormatException e) {
                            throw new NumberFormatException("Erro ao converter inteiro seção: traffic");
                        } catch (NullPointerException e) {
                            throw new NullPointerException("Erro de obrigatoriedade de campo: traffic");
                        }
                    }
                    if (nodeName.equals("virtual-topology")) {
                        simulation.setVirtualName(eElement.getAttribute("name"));
                    }
                    if (nodeName.equals("physical-topology")) {
                        try {
                            
                            simulation.setPhysicalName(eElement.getAttribute("name"));
                            simulation.setWavelengths(Integer.parseInt(eElement.getAttribute("wavelengths")));
                            NodeList physicalChildren = eElement.getChildNodes();
                            for (int j = 0; j < physicalChildren.getLength(); j++) {
                                if (physicalChildren.item(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE
                                        && physicalChildren.item(j).getNodeName().equals("nodes")) {
                                    
                                    NodeList nodes = physicalChildren.item(j).getChildNodes();
                                    
                                    for (int i = 0; i < nodes.getLength(); i++) {
                                        String name = nodes.item(i).getNodeName();
                                        if (nodes.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE
                                                && name.equals("node")) {
                                            
                                            Element element = (Element) nodes.item(i);
                                            int id = Integer.parseInt(element.getAttribute("id"));
                                            br.unb.cic.wdm.shared.domain.Node node = new br.unb.cic.wdm.shared.domain.Node(
                                                    id);
                                            node.setGroomingIn(new Integer(element.getAttribute("grooming-in-ports")));
                                            node.setGroomingOut(new Integer(element.getAttribute("grooming-out-ports")));
                                            node.setWlConverter(new Integer(element.getAttribute("wlconverters")));
                                            node.setConversionRange(new Integer(element
                                                    .getAttribute("wlconversion-range")));
                                            
                                            simulation.getNodes().add(node);
                                        }
                                    }
                                }
                                if (physicalChildren.item(j).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE
                                        && physicalChildren.item(j).getNodeName().equals("links")) {
                                    
                                    NodeList links = physicalChildren.item(j).getChildNodes();
                                    
                                    for (int i = 0; i < links.getLength(); i++) {
                                        if (links.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE
                                                && links.item(i).getNodeName().equals("link")) {
                                            
                                            Element element = (Element) links.item(i);
                                            Link link = new Link();
                                            
                                            link.setId(Integer.parseInt(element.getAttribute("id")));
                                            link.setTo(Integer.parseInt(element.getAttribute("destination")));
                                            link.setFrom(Integer.parseInt(element.getAttribute("source")));
                                            link.setDelay(Double.parseDouble(element.getAttribute("delay")));
                                            link.setBandwidth(Integer.parseInt(element.getAttribute("bandwidth")));
                                            link.setWeight(Integer.parseInt(element.getAttribute("weight")));
                                            simulation.getLinks().add(link);
                                        }
                                    }
                                }
                            }
                            
                        } catch (NumberFormatException e) {
                            throw new NumberFormatException("Erro ao converter inteiro seção: physical-topology");
                        } catch (NullPointerException e) {
                            throw new NullPointerException("Erro de obrigatoriedade de campo: physical-topology");
                        }
                    }
                    
                }
            }
            
        } catch (Exception e) {
            throw new ValidationException(e);
        }
        return simulation;
    }
    
}
