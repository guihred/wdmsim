package br.unb.cic.wdm.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.unb.cic.wdm.shared.domain.Simulation;

/**
 * Classe responsável por disponibilizar os
 * serviços REST que realizam operações de arquivo
 * 
 * 
 * @author Guilherme
 * 
 */
@Path("/file")
public class FileResource {
    
    /**
     * Recupera determinado objeto de simulação
     * guardado em banco
     * 
     * @param idSimulation
     * @return objeto de simulação salvo
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Simulation openSimulation(@PathParam("id") Long idSimulation) {
        Simulation simulation = SimulationUtils.getSimulations().get(idSimulation);
        return simulation;
    }
    
    /**
     * Salva determinada Simulação em banco e
     * retorna o seu id
     * 
     * @param simulation
     * @return idSimulation
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Long saveSimulation(Simulation simulation) {
        Simulation put = SimulationUtils.getSimulations().put(null, simulation);
        Long id = put.getId();
        System.out.println("SaveSimulation  id =" + id);
        return id;
    }
    
}
