package br.unb.cic.wdm.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unb.cic.wdm.shared.domain.Statistics;

/**
 * Servlet que permite recuperar resultados da
 * simulação executada
 * 
 * @author Guilherme
 * 
 */
public class DownloadResultsServlet extends HttpServlet {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String chave = req.getParameter("chave");
            resp.setContentType("application/octet-stream");
            
            if (req.getHeader("User-Agent").contains("MSIE")) {
                resp.setHeader("Content-Disposition", "inline;filename=" + "\"" + "default.txt" + "\"");
                resp.setHeader("Cache-Control", "no-cache");
            } else {
                resp.setHeader("Content-Disposition", "attachment;filename=" + "\"" + "default.txt" + "\"");
            }
            
            PrintWriter writer = resp.getWriter();
            if (chave != null) {
                Long id = Long.valueOf(chave);
                Statistics myStatistics = SimulationUtils.getSimulations().get(id).getStatistics().get(0);
                if (myStatistics != null) {
                    String result = myStatistics.getStatistics();
                    writer.write(result);
                    writer.flush();
                }
            }
            
            writer.close();
            // writer.close();
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
        
    }
    
}