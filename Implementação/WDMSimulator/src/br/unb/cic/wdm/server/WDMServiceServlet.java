package br.unb.cic.wdm.server;

import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * Classe de container responsável pelos serviços
 * REST
 * 
 * @author Guilherme
 * 
 */
public class WDMServiceServlet extends ServletContainer {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
}
