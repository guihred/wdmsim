package br.unb.cic.wdm.server;

import static java.util.Collections.singletonList;
import static javax.tools.JavaFileObject.Kind.SOURCE;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import sun.misc.Unsafe;
import br.unb.cic.wdm.server.wdmsim.rwa.RWA;

/**
 * Classe que disponibiliza os serviços REST de
 * acesso às classes RWA
 * 
 * @author Guilherme
 * 
 */
@SuppressWarnings("restriction")
@Path("/rwa")
public class RwaResource {
    
    /**
     * Nome do pacote onde estão contidos as
     * classes RWA
     */
    private static final String PACKAGE_NAME = "br.unb.cic.wdm.server.wdmsim.rwa";
    
    /**
     * Realiza a compilação da classe desenvolvida
     * pelo usuário, checa se há erros, salva a
     * classe e retorna lista com o diagnóstico da
     * compilação
     * 
     * @param customRwa
     * @return lista
     */
    @SuppressWarnings({ "unchecked" })
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> addCustomRwa(String customRwa) {
        final List<String> diagnosticMsg = new ArrayList<String>();
        final StringBuilder source = new StringBuilder(customRwa);
        
        if (!source.toString().contains("package")) {
            source.insert(0, "package " + PACKAGE_NAME + ";\n\n");
        }
        int indexOf = source.toString().indexOf("class");
        int indexOf2 = source.toString().indexOf("{", indexOf);
        String className = source.toString().substring(indexOf, indexOf2).split(" ")[1];
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        
        final SimpleJavaFileObject simpleJavaFileObject = new SimpleJavaFileObject(URI.create(className + ".java"),
                SOURCE) {
            
            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors) {
                return source.toString();
            }
            
            @Override
            public OutputStream openOutputStream() throws IOException {
                return byteArrayOutputStream;
            }
        };
        
        final Locale pt = new Locale("pt");
        DiagnosticListener<JavaFileObject> diagnosticListener = new DiagnosticListener<JavaFileObject>() {
            
            @Override
            public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
                String message = diagnostic.toString();
                diagnosticMsg.add(message);
            }
            
        };
        Properties prop= new Properties();
        try {
			prop.load(new FileReader(new File("jdk.properties")));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.setProperty("java.home",prop.getProperty("java.home"));
	
        StandardJavaFileManager standardFileManager = ToolProvider.getSystemJavaCompiler().getStandardFileManager(
                diagnosticListener, pt, Charset.defaultCharset());
        final JavaFileManager javaFileManager = new ForwardingJavaFileManager<StandardJavaFileManager>(
                standardFileManager) {
            
            @Override
            public JavaFileObject getJavaFileForOutput(Location location, String className, JavaFileObject.Kind kind,
                    FileObject sibling) throws IOException {
                return simpleJavaFileObject;
            }
        };
        
        ToolProvider.getSystemJavaCompiler()
                .getTask(null, javaFileManager, diagnosticListener, null, null, singletonList(simpleJavaFileObject))
                .call();
        
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        
        // use the unsafe class to load in the
        // class bytes
        Class<?> aClass = null;
        try {
            final Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            final Unsafe unsafe = (Unsafe) f.get(null);
            aClass = unsafe.defineAnonymousClass(getClass(), bytes, null);
            
            final Object o = aClass.newInstance();
            if (!(o instanceof RWA)) {
                diagnosticMsg.add("Classe não implementa a interface RWA.");
            } else {
                SimulationUtils.getCustomRwas().add((Class<? extends RWA>) aClass);
                diagnosticMsg.add("Classe Adicionada com sucesso");
            }
        } catch (Throwable e) {
            if (e instanceof LinkageError) {
                diagnosticMsg.add("Classe já adicionada");
            } else {
                diagnosticMsg.add(e.getLocalizedMessage());
            }
            
        }
        return diagnosticMsg;
    }
    
    /**
     * @return lista de classes RWA disponíveis
     *         para simulação
     */
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getRwaOptions() {
        
        List<String> arrayList = new ArrayList<String>();
        try {
            List<Class<?>> classes = SimulationUtils.getClassesFromPackage(PACKAGE_NAME);
            classes.addAll(SimulationUtils.getCustomRwas());
            for (Class<?> class1 : classes) {
                if (!class1.isInterface()) {
                    arrayList.add(class1.getSimpleName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return arrayList;
    }
    
}