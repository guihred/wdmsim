package br.unb.cic.wdm.server;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import br.unb.cic.wdm.shared.domain.Calls;
import br.unb.cic.wdm.shared.domain.Link;
import br.unb.cic.wdm.shared.domain.Node;
import br.unb.cic.wdm.shared.domain.Simulation;
import br.unb.cic.wdm.shared.domain.Statistics;

//@SuppressWarnings("unchecked")
/**
 * Classe de acesso ao banco de dados
 * 
 * @author Guilherme
 */
@SuppressWarnings("unchecked")
public class SimulationDaoMap {
    
    // private static ObjectifyFactory factory =
    // new ObjectifyFactory();
    
    /**
     * Responsável por criar e gerenciar sessões
     * de acesso ao banco de dados
     */
    protected static SessionFactory sessionFactory;
    static {
        try {
            Configuration configuration = new AnnotationConfiguration();
            configuration.addAnnotatedClass(Node.class);
            configuration.addAnnotatedClass(Link.class);
            configuration.addAnnotatedClass(Calls.class);
            configuration.addAnnotatedClass(Simulation.class);
            configuration.addAnnotatedClass(Statistics.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().build();
            sessionFactory = configuration.configure().buildSessionFactory(serviceRegistry);
            System.out.println("Session Factory created");
            
        } catch (Throwable e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * Limpa informações do banco
     */
    public void clear() {
        Session session = getSession();
        List<Simulation> list = session.createCriteria(Simulation.class).list();
        for (Simulation e : list) {
            session.delete(e);
        }
    }
    
    /**
     * Limpa ids para evitar cascateamentos
     * incorretos
     * 
     * @param value
     */
    private void clearIds(Simulation value) {
        for (Calls c : value.getCalls()) {
            c.setId(null);
        }
        for (Node n : value.getNodes()) {
            n.setCodigo(null);
        }
        for (Link l : value.getLinks()) {
            l.setCodigo(null);
        }
    }
    
    /**
     * Recupera determinada simulação a partir de
     * sua chave primária
     * 
     * @param key
     * @return simulação salva
     */
    public Simulation get(Long key) {
        Simulation v = null;
        try {
            Session session = getSession();
            Transaction beginTransaction = session.beginTransaction();
            v = (Simulation) session.get(Simulation.class, key);
            beginTransaction.commit();
            System.out.println(v);
            return v;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * @return sessão atual de acesso ao banco de
     *         dados
     */
    protected Session getSession() {
        try {
            Session openSession = sessionFactory.openSession();
            return openSession;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        
        return null;
    }
    
    /**
     * @return novo id de simulação disponível
     *         para upload
     */
    public Long newId() {
        Session session = getSession();
        Transaction beginTransaction = session.beginTransaction();
        Simulation simulation = new Simulation();
        Long key = (Long) session.save(simulation);
        beginTransaction.commit();
        return key;
    }
    
    /**
     * Realiza o salvamento de dados de simulação
     * no banco
     * 
     * @param key
     * @param value
     * @return simulação salva
     */
    public Simulation put(Long key, Simulation value) {
        System.out.println("Put key =" + key + " value=" + value);
        clearIds(value);
        Session session = getSession();
        Transaction beginTransaction = session.beginTransaction();
        if (key == null) {
            session.save(value);
        } else {
            value.setId(key);
            for (Calls call : value.getCalls()) {
                session.saveOrUpdate(call);
            }
            for (Link call : value.getLinks()) {
                session.saveOrUpdate(call);
            }
            for (Node call : value.getNodes()) {
                session.saveOrUpdate(call);
            }
            if (value.getStatistics() != null)
                for (Statistics stat : value.getStatistics()) {
                    session.saveOrUpdate(stat);
                }
            
            session.saveOrUpdate(value);
            
        }
        beginTransaction.commit();
        session.flush();
        return value;
    }
}
