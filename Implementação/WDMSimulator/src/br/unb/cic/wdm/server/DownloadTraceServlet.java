package br.unb.cic.wdm.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.unb.cic.wdm.server.wdmsim.SimulatorGWT;
import br.unb.cic.wdm.server.wdmsim.Tracer;
import br.unb.cic.wdm.shared.domain.Statistics;

/**
 * Servlet que permite ao usuário realizar
 * download de arquivo de trace
 * 
 * @author Guilherme
 * 
 */
public class DownloadTraceServlet extends HttpServlet {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Long chave = Long.valueOf(req.getParameter("chave"));
            int BUFFER = 1024 * 100;
            resp.setContentType("application/octet-stream");
            
            if (req.getHeader("User-Agent").contains("MSIE")) {
                resp.setHeader("Content-Disposition", "inline;filename=" + "\"" + "default.trace" + "\"");
                resp.setHeader("Cache-Control", "no-cache");
            } else {
                resp.setHeader("Content-Disposition", "attachment;filename=" + "\"" + "default.trace" + "\"");
            }
            
            PrintWriter writer = resp.getWriter();
            List<Statistics> statistics = SimulationUtils.getSimulations().get(chave).getStatistics();
            if (statistics != null) {
                
                SimulatorGWT wdm = new SimulatorGWT();
                resp.setStatus(HttpServletResponse.SC_ACCEPTED);
                resp.setBufferSize(BUFFER);
                wdm.setTracer(new Tracer());
                wdm.setTracerFile(writer);
                wdm.Execute(chave, true, true, 0, 5);
                // List<Statistics> arrayList =
                // new ArrayList<Statistics>();
                // arrayList.add(wdm.getStats().asStatistics());
                // SimulationUtils.getSimStatistics().put(chave,
                // arrayList);
            } else {
                writer.close();
            }
            // writer.flush();
            // writer.close();
        } catch (Exception ex) {
        	ex.printStackTrace();
            throw new ServletException(ex);
        }
        
    }
    
}